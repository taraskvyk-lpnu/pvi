    function addRow(group, name, surname, gender, birthday) {
        var table = document.getElementById('student-table');
        var newRow = table.insertRow(-1); 
        
        var cell1 = newRow.insertCell(0);
        cell1.style.width = '5%';

        var checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        cell1.appendChild(checkbox); 

        var fullname = name + ' ' + surname;
        var rowData = [group, fullname, gender, birthday];

        for (var i = 1; i < rowData.length + 1; i++) {
            var cell = newRow.insertCell(i);
            cell.innerHTML = rowData[i - 1];
        }

        var statusFieldCell = newRow.insertCell(5);

        statusFieldCell.innerHTML = `
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="${table.rows.length % 2 == 0 ? 'grey' : 'green'}" fill-opacity="0.25" class="bi bi-circle-fill" viewBox="0 0 16 16">
                <circle cx="8" cy="8" r="8"/>
            </svg>
        `;

        var deleteButtonCell = newRow.insertCell(6);
        var deleteButton = document.createElement('div');
        deleteButton.classList.add('options-div');

        deleteButton.innerHTML = `
            <button title="Edit" style="text-decoration: none; color: black; padding: 5%;">
                <svg xmlns="http://www.w3.org/2000/svg" width="25%" height="25" fill="currentColor" class="bi bi-pencil-square edit" viewBox="0 0 16 16">
                    <path id="pencil" d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                </svg>
            </button>

            <button title="Delete" style="color: red; padding: 5%;" id="deleteButton" onclick="deleteRow(this)">
                <svg xmlns="http://www.w3.org/2000/svg" width="25%" height="25" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16">
                    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2z"/>
                    <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708"/>
                </svg>
            </button>
        `;

        deleteButton.onclick = function() { deleteRow(deleteButton); };
        deleteButtonCell.appendChild(deleteButton);
    }

    function toggleInputForm(isSaveBtn) {
        var overlay = document.getElementById("overlay");
        var centeredDiv = document.getElementById("form-container");
      
        if (overlay.style.display === "block") {
            
            var groupSelection = document.getElementById('groupSelect');
            var group = groupSelection.selectedOptions[0].value;
            
            var name = document.getElementById('nameInput').value;
            var surname = document.getElementById('surnameInput').value;
            
            var genderSelect = document.getElementById('genderSelect');
            var gender = genderSelect.selectedOptions[0].value;
            var birthday = document.getElementById('birthdayInput').value;

            if(group == '')
                return;
            
            if(name == '')
                return;
            

            if(gender == '')
                return;

        
            if(birthday == '')
                return;
            else {
                var dateParts = birthday.split('-');

                var day = dateParts[2];
                var month = dateParts[1];
                var year = dateParts[0];

                var formattedDate = day + '.' + month + '.' + year;
            }

            overlay.style.display = "none";
            centeredDiv.style.display = "none";
            
            if (isSaveBtn) {
                groupSelection.selectedIndex = 0; 
                nameInput.value = ""; 
                genderSelect.selectedIndex = 0; 
                birthdayInput.value = ""; 
                addRow(group, name, surname, gender, formattedDate);
            }
            
        } else {
          overlay.style.display = "block";
          centeredDiv.style.display = "block";
        }
    }

    function closeInputForm() {

        var li = document.createElement('li');
        li.innerHTML = 
        ul.appendChild(li);

        var centeredDiv = document.getElementById("form-container");
      
        centeredDiv.style.display = "none";
        disableOverlay();

        groupSelect.selectedIndex = 0; 
        nameInput.value = ""; 
        genderSelect.selectedIndex = 0; 
        birthdayInput.value = ""; 
    }

    function disableOverlay()
    {
        var overlay = document.getElementById("overlay");
        overlay.style.display = "none";
    }

    function enableOverlay()
    {
        var overlay = document.getElementById("overlay");
        overlay.style.display = "block";
    }

    function showWarningWindow(row) {
        var warningContainer = document.getElementById('warning-container');
        warningContainer.style.display = 'block';
    
        var warningMessage = document.getElementById('warning-message');
        var studentName = ''; 
    
        if (row) {
            var cells = row.getElementsByTagName('td');
            if (cells.length >= 3) { 
                studentName = cells[2].innerText;
            }
        }
    
        warningMessage.innerHTML = '<p> Are you really want to delete ' + studentName + '?</p>';
        warningContainer.dataset.rowIndex = getIndex(row); 
    }
    
    function deleteRow(button) {
        var row = button.closest('tr');
        enableOverlay();
        showWarningWindow(row);
    }

    function confirmDelete() {
        var rowIndex = document.getElementById('warning-container').dataset.rowIndex;
    
        if (rowIndex !== undefined) {
            var table = document.getElementById('student-table');
            var row = table.rows[parseInt(rowIndex)];
            row.remove(); 
        } else {
            console.error('Рядок не знайдений.');
        }
    
        hideWarningWindow();
        disableOverlay();
    }
    
    function getIndex(row) {
        var table = row.parentNode;
        var index = 0;
        while ((row = row.previousElementSibling) != null) {
            index++;
        }
        return index;
    }

    function toggleLogin() {
        var loginLink = document.getElementById('login-link');
        var userName = document.getElementById('user-name');

        if(loginLink.innerHTML == 'Log In') {
            userName.innerHTML = 'Taras Kvyk';
            userName.style.display = 'flex';
            loginLink.innerHTML = 'Log Out';
        }
        else {
            userName.innerHTML = '_____';
            loginLink.innerHTML = 'Log In';
        }
    }

    function hideWarningWindow()
    {
        document.getElementById('warning-container').style.display = 'none';
        disableOverlay();
    }

    function processAllCheckboxes()
    {
        var mainCheckbox = document.getElementById('main-checkbox');
        var checkboxes = document.querySelectorAll('table input[type="checkbox"]');

        if(mainCheckbox.checked)
        {
            checkboxes.forEach(function(checkbox) {
                checkbox.checked = true;
            });
        }
        else
        {
            checkboxes.forEach(function(checkbox) {
                checkbox.checked = false;
            });
        }
    }

    document.addEventListener('DOMContentLoaded', function() {
        var navLinks = document.querySelectorAll('nav ul li a');
    
        navLinks.forEach(function(link) {
            link.addEventListener('click', function() {
                this.classList.add('active');
    
                navLinks.forEach(function(otherLink) {
                    if (otherLink !== link) {
                        otherLink.classList.remove('active');
                    }
                });
            });
        });
    });

    function bellBtnClicked() {
        var dropdown = document.getElementById('notification-dropdown');
        dropdown.style.display = 'block';

        var redCircle = document.getElementById('bell-circle');
        redCircle.style.display = 'none';
    }

    function showNotification() {
        hideUserMenu();
        var addRowBtn = document.getElementById('addRow');
        addRowBtn.style.position = 'relative';

        var dropdown = document.getElementById('notification-dropdown');
        dropdown.style.display = 'block';
    }

    function hideNotifications() {
        var dropdown = document.getElementById('notification-dropdown');
        dropdown.style.display = 'none';
        
        var addRowBtn = document.getElementById('addRow');
        addRowBtn.style.position = 'static';
    }

    function showUserMenu() {
        hideNotifications();
        var addRowBtn = document.getElementById('addRow');
        addRowBtn.style.position = 'relative';
        
        var dropdown = document.getElementById('user-menu-dropdown');
        dropdown.style.display = 'block';
    }

    function hideUserMenu() {
        var dropdown = document.getElementById('user-menu-dropdown');
        dropdown.style.display = 'none';

        var addRowBtn = document.getElementById('addRow');
        addRowBtn.style.position = 'static';
    }
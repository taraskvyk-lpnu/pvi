function OpenSignUpForm() {  
    var centeredDiv = $("#form-SignUp");
    centeredDiv.css("display", "block");
    CloseSignInForm();
    enableOverlay();
}

function СloseSignUpForm() {  
    var centeredDiv = $("#form-SignUp");
    centeredDiv.css("display", "none");
    $('.errorSpan').hide();
    disableOverlay();
}

function enableOverlay()
{
    var overlay = $("#overlay");
    overlay.css("display", "block");
}

function OpenSignInForm() {
    var centeredDiv = $("#form-SignIn");
    centeredDiv.css("display", "block");
    $('.errorSpan').hide();
    СloseSignUpForm();
    enableOverlay();
}

function CloseSignInForm() {
    var centeredDiv = $("#form-SignIn");
    centeredDiv.css("display", "none");
    $('.errorSpan').hide();
    disableOverlay();
}

function SaveSignInForm(){
    var checkboxValue = $('#signIn-ckeckbox').val();
    if(!checkboxValue == 'on'){
        $('#signIn-checkbox-error').css('dispay', 'block');
        $('#signIn-checkbox-error').text('You must agree');
        return;
    }

    var saved = true;

    var login = $('#form-SignIn #signInloginInput').val();
    var password = $('#form-SignIn #passwordInput').val();
    
    var userData = {
        login: login,
        password: password
    };
   
    if(!isValidSignInData(userData)){
        alert("Invalid data");
        saved = false;
    }

    // POST REQUEST
    if(saved){
        $('#form-SignIn #signInloginInput').text('');
        $('#form-SignIn #passwordInput').text('');
        CloseSignInForm();
    }

    authenticateUser(userData.login, userData.password);
}

function SaveSignUpForm(){
    var checkboxValue = $('#signUp-ckeckbox')[0];
    if(!checkboxValue.checked){
        $('#signUp-checkbox-error').css('dispay', 'block');
        $('#signUp-checkbox-error').text('You must agree');
        return;
    }

    var saved = true;

    var login = $('#form-SignUp #signUploginInput').val();
    var password = $('#form-SignUp #passwordSignUp').val();
    var passwordConfirm = $('#form-SignUp #repeatPasswordSignUp').val();
    var birthday = $('#form-SignUp #birthdayInput').val();
   
    var userData = {
        login: login,
        password: password, 
        passwordConfirm: passwordConfirm, 
        birthday: birthday
    };
   
    if(!isValidSignUpData(userData)){
        alert("Invalid data");
        saved = false;
    }

    // POST REQUEST
    if(saved){
        alert('saves');
        $('#form-SignUp #signUploginInput').text('');
        $('#form-SignUp #passwordSignUp').text('');
        $('#form-SignUp #repeatPasswordSignUp').text('');
        $('#form-SignUp #birthdayInput').text('');
        СloseSignUpForm();
    }
}

function disableOverlay()
{
    var overlay = $("#overlay");
    overlay.css("display", "none");
}

function toggleSaveBtn(checkbox){
    $('.saveBtn svg').css('fill', checkbox.checked ? 'green' : 'black');
}

function isValidSignUpData(data){
    var isValid = true;
    
    if(!isValidLogin(data.login)){
        isValid = false;
    }

    if(!isValidPassword(data.password)){
        isValid = false;
    }

    if(!isValidPassword(data.passwordConfirm)){
        isValid = false;
    }

    if(!ConfirmPassword(data.passwordConfirm)){
        isValid = false;
    }

    if(!isValidDate(data.birthday)){
        isValid = false;
    }

    return isValid;
}

function isValidSignInData(data){
    var isValid = true;
    
    if(!isValidLogin(data.login)){
        isValid = false;
    }

    if(!isValidPassword(data.password)){
        isValid = false;
    }

    return isValid;
}

function isValidDate(birthDate)
{
    if (!birthDate) {
        $(`.birthday-error`).css('display', 'block');
        $(`.birthday-error`).text(`Birthday field cannot be empty!`);
        return false;
    }

    const parts = birthDate.split('-');
    const year = parseInt(parts[0], 10);

    const currentYear = new Date().getFullYear();
    if (year < 1990 || year > 2006 || year > currentYear) {
        $(`.birthday-error`).css('display', 'block');
        $(`.birthday-error`).text(`Birthday date should be from 1990 to 2006!`);
        return false;
    }
    else
    {
        $(`.birthday-error`).text('');
        return true;
    }
}

function isValidLogin(login) {
    var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    
    if (emailRegex.test(login)) {
        $(`.login-error`).text('');
        return true; 
    } else {
        $(`.login-error`).css('display', 'block');
        $(`.login-error`).text(`Incorrect email format`);
        return false;
    }
}

function ConfirmPassword(passwordToConfirm) {

    var password = $('#passwordSignUp').val();

    if (passwordToConfirm !== password) {
        
        $(`#repeatPassword-error`).css('display', 'block');
        $(`#repeatPassword-error`).text(`Passwords do not match`);
        return false;
    } 
    else{
        $(`#repeatPassword-error`).css('display', 'none');
        $(`#repeatPassword-error`).text('');
    }
    
    $(`#repeatPassword-error`).text(` `);
    return true;    
}

function isValidPassword(password) {
    if (password.length < 8) {
        $('#passwordSignIn-error, #passwordSignUp-error').css('display', 'block');
        $('#passwordSignIn-error, #passwordSignUp-error').text('Password must contain at least 8 symbols');
        return false;
    }else {
        $('#passwordSignIn-error, #passwordSignUp-error').css('display', 'none');
        $('#passwordSignIn-error, #passwordSignUp-error').text('');
    }

    var uppercaseRegex = /[A-ZА-Я]/;
    var digitRegex = /\d/;

    if (!uppercaseRegex.test(password)) {
        $('#passwordSignIn-error, #passwordSignUp-error').css('display', 'block');
        $('#passwordSignIn-error, #passwordSignUp-error').text(`Password must contain upper case char`);
        return false;
    }else {
        $('#passwordSignIn-error, #passwordSignUp-error').css('display', 'none');
        $('#passwordSignIn-error, #passwordSignUp-error').text('');
    }

    if (!digitRegex.test(password)) {
        $('#passwordSignIn-error, #passwordSignUp-error').css('display', 'block');
        $('#passwordSignIn-error, #passwordSignUp-error').text(`Password must contain a digital char`);
        return false;
    }else {
        $('#passwordSignIn-error, #passwordSignUp-error').css('display', 'none');
        $('#passwordSignIn-error, #passwordSignUp-error').text('');
    }

    return true;
}

/*
function getUserNameFromLocalStorage(){
    var username = localStorage.getItem('username');

    if (username) {
        return username;
    } 
    
    return 'Anonymous';
}

document.addEventListener('DOMContentLoaded', function() {
    var username = getUserNameFromLocalStorage();
    document.getElementById('user-name').textContent = username;
});

document.addEventListener('keydown', function(event) {
    if (event.ctrlKey && event.keyCode === 116) {
        localStorage.clear();
    }
});*/
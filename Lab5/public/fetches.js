function setupChatItemClickHandler(li){
    var chatName = $(li).data("chat-name");
    var previousRoomName = $("#roomName").text();

    if(chatName === previousRoomName){
        return;
    }
    
    $("#roomName").text(chatName);
    $(".own-message .font-weight-bold").text("You");
    
    // Виконання запиту для маршруту /:room
    fetch(`http://localhost:3002/${chatName}`, {
        method: 'GET',
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        }})
        .then(response => {
            if (response.status === 200) {
                document.getElementById('messageForm').removeAttribute('hidden');
                return response.json();
            } else if (response.status === 401) {
                alert('Unauthorized!\nLog in to use chat');
                document.getElementById('messageForm').setAttribute('hidden', true);
            } else {
                // Інші помилки, наприклад, серверна помилка
                alert('Login failed'); 
                document.getElementById('messageForm').setAttribute('hidden', true);
            }
        })
        .then(data => {
            console.log('Response:', data);
            
            const sessionID = sessionStorage.getItem('sessionId');
            const users = data.users;
            console.log("users : " + users);
            fillConnectedUsers(users);
            socket.emit('disconnect-from-room', previousRoomName, sessionID);
            // Додаємо клас 'active' до вибраного чату і видаляємо його з інших
            $(".chat-item").removeClass("active");
            $(li).addClass("active");
            
            //alert("Handler " + sessionID)
            socket.emit('new-user', chatName, sessionID);
            $("#chat-messages").empty();
            addCenterMessage(`You joined ${chatName}`);
            //addChatMemberToUI("You");
        })
        .catch(error => {
            console.error('Error fetching room:', error);
        });
}

function fillConnectedUsers(users) {
    const chatHeader = document.getElementById('chat-header');

    chatHeader.innerHTML = '';

    users.forEach(user => {
        addChatMemberToUI(user);
    });
}

function addChatMemberToUI(name){
    var userElement = 
    `
    <div class="d-inline-block text-center m-0 connected-user" style="max-height: 30px;">
        <i class="bi bi-person" style="color: rgb(0, 0, 0);"></i>
        <p class="mt-0 ml-1 text-center font-size-small">${name}</p>
    </div>
    `;

    const chatHeader = document.getElementById('chat-header');
    chatHeader.innerHTML +=userElement;
}

function removeChatMemberFromUI(name) {
    const chatHeader = document.getElementById('chat-header');
    const userElements = chatHeader.getElementsByClassName('connected-user');

    for (let i = 0; i < userElements.length; i++) {
        const userNameElement = userElements[i].querySelector('.font-size-small');
        if (userNameElement.textContent == name) {
            const userElement = userElements[i];
            userElement.parentNode.removeChild(userElement);
            break; 
        }
    }
}

async function authenticateUser(login, password) {
    let userName = '';
    await $.ajax({
        url: 'http://localhost:8080/lab4/php/signIn.php',
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({ login: login, password: password }),
        success: function(data) {
            if (data.success) {
                userName = data.name; 
                localStorage.setItem(login, userName);
                sessionStorage.setItem('user_id', data.user_id);
                sessionStorage.setItem('userName', userName);

                $('#user-name').text(userName);
                console.log('User authenticated successfully');
            } else {
                alert('Invalid username or password');
                console.log('Invalid username or password');
            }
        },
        error: function(xhr, status, error) {
            //$('#user-name').text(login);
            const response = JSON.parse(xhr.responseText);
        
           // alert(response.error);
            console.error('AJAX request failed:', error);
        }
    });

    const user_id = sessionStorage.getItem('user_id');
    
    fetch('http://localhost:3002/login', {
            method: 'POST',
            credentials: "include",
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({ login: login, username: userName, user_id : user_id})
        })
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                console.error('Login failed');
            }
        })
        .then(data => {
            if (data.sessionId) {
                // Store session ID in sessionStorage
                sessionStorage.setItem('sessionId', data.sessionId);
                console.log('Session ID stored in sessionStorage:', data.sessionId);
            } else {
                console.error('Session ID not found in response');
            }
        })
        .catch(error => console.error('Error:', error));
}
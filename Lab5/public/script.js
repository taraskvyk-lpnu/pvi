const socket = io('http://localhost:3002', { transports : ['websocket'] })

let room = $('#roomName').text();
const name = $('#user-name').text();

socket.on('chat-message', data => {
    addAnotherMessage(data);
})

socket.on('user-connected', data => {
    addCenterMessage(data + ' joined');
    //addChatMemberToUI(data);
})

socket.on('added-by-another-to-room', (roomName) => {
    alert("Add")
    addToRoom(roomName);
    alert("Added")
})

function addToRoom(chatName){
    fetch(`http://localhost:3002/${chatName}`, {
        method: 'GET',
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        }
        })
        .then(response => {
            if (response.status === 200) {
                document.getElementById('messageForm').removeAttribute('hidden');
                return response.json();
            } else if (response.status === 401) {
                alert('Unauthorized!\nLog in to use chat');
                document.getElementById('messageForm').setAttribute('hidden', true);
            } else {
                alert('Login failed'); 
                document.getElementById('messageForm').setAttribute('hidden', true);
            }
        })
        .then(data => {
            console.log('Response:', data);
            
            const users = data.users;
            fillConnectedUsers(users);
            const li = document.querySelector(`li[data-chat-name="${chatName}"]`);

            $(".chat-item").removeClass("active");
            $(li).addClass("active");

            const sessionID = sessionStorage.getItem('sessionId');
            socket.emit('new-user', chatName, sessionID);
            $("#chat-messages").empty();
            addCenterMessage(`You joined ${chatName}`);
        })
        .catch(error => {
            console.error('Error fetching room:', error);
        });
}

socket.on('user-disconnected', name => {
    addCenterMessage(name + ' disconnected');
    removeChatMemberFromUI(name);
})

socket.on('room-created', roomName => {
    console.log('room created ' + roomName);
})

$("#messageForm").submit(function(event) {
    event.preventDefault();
    ownMessage();    
});

function ownMessage(){
    var message = $("#messageInput").val();

    if (message.trim() !== "") {
      var ownMessage = `
      <div class="own-message mb-2">
            <div class="card-no-border">
                <div class="card-body text-right">
                    <div class="border border-secondary p-2 rounded d-inline-block">
                        <div class="d-flex align-items-center justify-content-end">
                            <i class="bi bi-person" style="color: rgb(200, 196, 196);"></i>
                            <p class="mb-0 font-weight-bold" style="color: rgb(200, 196, 196); font-size: 13px;">You</p>
                        </div>
                        <p class="card-text">${message}</p>
                    </div>
                </div>
            </div>
        </div>
        `;
        
        let roomName = $('#roomName').text();
       
        $("#chat-messages").append(ownMessage);
        const sessionID = sessionStorage.getItem('sessionId');
        socket.emit('send-chat-message', roomName, message, sessionID)
        $("#messageInput").val("");
        scrollToBottom();
    }
}

function addAnotherMessage(data){
    var anotherMessage = `
        <div class="other-message mb-2">
            <!-- Other user's message -->
            <div class="card-no-border m-0">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <img src="https://via.placeholder.com/50" style="width: 50px; height: 50px;" class="rounded-circle mr-2 user-img" alt="User 2">
                        <div class="border border-secondary p-2 rounded">
                            <div class="d-flex align-items-center justify-content-start">
                                <i class="bi bi-person" style="color: rgb(200, 196, 196);"></i>
                                <p class="mb-0 font-weight-bold" style="color: rgb(200, 196, 196); font-size: 13px;">${data.name}</p>
                            </div>
                            <p class="mb-0">${data.message}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
      $("#chat-messages").append(anotherMessage);
      scrollToBottom();
}

function addCenterMessage(message) {
    var centerMessage = `
    <div class="mb-2">
        <!-- Other user's message -->
        <div class="card-no-border m-0">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-center">
                    <div class="d-flex align-text-center border border-secondary p-1 rounded">
                        <p class="mb-0">${message}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `;

    $("#chat-messages").append(centerMessage);
    scrollToBottom();
}

function scrollToBottom() {
    var chatBody = $("#chat-messages");
    chatBody.scrollTop(chatBody.prop("scrollHeight"));
}

async function loadChatList(){
    fetch('http://localhost:3002/rooms', {
        method: "GET",
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response =>{
            return response.json()
        } )
        .then(function(rooms) {
            console.log("Rooms : " + rooms);
            document.querySelector('.list-group').innerHTML = '';
            rooms.forEach(room => {
                const li = `
                <li class="list-group-item chat-item" data-chat-name="${room.name}" data-username="${room.name}" onclick="setupChatItemClickHandler(this)">
                    <img src="https://via.placeholder.com/50" style="width: 50px; height: 50px;" class="img-fluid rounded-circle mr-2" alt="${room.name}"> ${room.name}
                </li>
                `;
                document.querySelector('.list-group').innerHTML += li;
            });
        })
        .catch(error => console.error('Error fetching rooms:', error));
}

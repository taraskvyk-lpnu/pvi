const express = require('express');
const session = require('express-session');
const app = express();
const server = require('http').Server(app)
const io = require('socket.io')(server)
const cors = require('cors');
require('dotenv').config();

app.set('views', './views')
app.use(express.static('public'))
app.use(express.urlencoded({extended: true}))
app.use(express.json());

const cookieParser = require('cookie-parser');
app.use(cookieParser());

app.use(
    cors({
      origin: "http://localhost:8080",
      optionsSuccessStatus: 200,
      credentials: true,
    })
  );

app.use(
session({
    secret: 'fdiohrrhoelaeir',
    resave: false,
    saveUninitialized: true,
    cookie: {
    expires: Date.now() + 1000 * 60 * 60 * 24,
    maxAge: 1000 * 60 * 60 * 24,
    httpOnly: true,
    },
})
);

const rooms = {
    room1: {
        name: 'room1',
        users: {}
    },
    room2: {
        name: 'room2',
        users: {}
    },
};

const connectedUsers = {};

app.get('/rooms', (req, res) => {

    if (!req.cookies || !req.cookies.sessionId) {
        res.json(Object.values({}));
        return; 
    }   

    const accessibleRooms = getRoomsForUser(req.cookies.sessionId);
    res.send(Object.values(accessibleRooms));
});

function getRoomsForUser(sessionId) {
    const accessibleRooms = [];
    
    for (const roomName in rooms) {
        if (rooms.hasOwnProperty(roomName)) {
            const usersInRoom = rooms[roomName].users;
            
            if (usersInRoom.hasOwnProperty(sessionId)) {
                accessibleRooms.push(rooms[roomName]);
            }
        }
    }
    
    return accessibleRooms;
}

app.get('/connectedUsers', (req, res) => {
    res.json(connectedUsers);
});

const formidable = require('formidable');

app.post('/room', (req, res) => {
    console.log('entered post room');

    const form = new formidable.IncomingForm();
    form.parse(req, (err, fields, files) => {
        if (err) {
            console.error('Error parsing form data:', err);
            return res.status(500).send('Error parsing form data');
        }

        const { room: room, selectedKeys } = fields;
        roomName = room[0];
        const sessionsArr = selectedKeys[0];
      
        if (sessionsArr && selectedKeys.length > 0) {
            let selectedUsers;
            selectedUsers = sessionsArr.split(',');
        
            if (rooms[roomName]) {
                return res.sendStatus(404);
            } else {
                const roomUsers = {};

                selectedUsers.forEach(function(key) {
                    if (!roomUsers.hasOwnProperty(key)) {
                        roomUsers[key] = connectedUsers[key];
                    }
                });

                rooms[roomName] = { name: roomName, users: roomUsers };
                res.sendStatus(200);
                io.emit('room-created', roomName);
            }
        } else {
            console.error('selectedKeys property is missing or empty');
            res.status(400).send('selectedKeys property is missing or empty');
        }
    });
});

app.get('/:room', (req, res) => {
    const room = req.params.room;

    if(rooms[room] != null){
        const users = rooms[room].users;
        console.log(users);
        console.log(Object.values(users));
        return res.json({
            url : 'http://localhost:8080/lab4/views/chat.html?roomName=' + room,
            users : Object.values(users)});
        }else {

        return res.status(404).send('Room not found');
    }
})

app.use(function auth(req, res, next) {
    
    console.log("auth", req.session.id, req.url);
    next();
  });

io.on('connection', socket => {
    socket.on('new-user', (roomName, sessionID) => { 

        if(!connectedUsers[sessionID]){  
            return;
        }

        const name = connectedUsers[sessionID];
        
        if(rooms[roomName] != null){

            if (rooms[roomName].users.hasOwnProperty(socket.id)) {              
                socket.broadcast.to(roomName).emit('user-connected', name);
                return;
            }

            socket.join(roomName)
            connectedUsers[sessionID] = name;
            console.log("Socket " + socket.id);
            socket.broadcast.to(roomName).emit('user-connected', name);
        }
        else{
            console.log(name + ' not connected');
        }
    });

    socket.on('send-chat-message', (room, message, sessionId) => {    
        if (room && rooms[room] && socket.to(room)) {
            socket.broadcast.to(room).emit('chat-message', { message: message, name: rooms[room].users[sessionId] });
            console.log("sended message")
        } else {    
            console.log("Error: Invalid room or socket");
        }
    })

    socket.on('disconnect', () => {        
        const cookiesHeader = socket.request.headers.cookie; 
        let sessionId;
        if (cookiesHeader) {
            const cookies = cookiesHeader.split(';'); 

            for (const cookie of cookies) {
                const [name, value] = cookie.split('='); 

                if (name.trim() === 'sessionId') {
                    sessionId = value.trim(); 
                    console.log('Session ID:', sessionId);
                    break;
                }
            }
        }
        
        console.log(connectedUsers)
      
        /*
        for (let key in connectedUsers) {
            
            console.log(key + " key")
            console.log(sessionId + " sessionId")
            if (key === sessionId) {
                delete connectedUsers[sessionId];
                break;
            }
        }*/

        console.log(connectedUsers)
    })

    socket.on('disconnect-from-room', (room, sessionID) => {   
        socket.leave(room);
        
        if(rooms[room] != null){
            console.log(rooms[room].users[sessionID] + ' disconnected');
            socket.broadcast.to(room).emit('user-disconnected', rooms[room].users[sessionID])
            //delete rooms[room].users[sessionID]
            console.log(rooms);
        }
    })
})

app.post('/login', (req, res) => {
    const { login, username, user_id } = req.body;
    req.session.login = login;
    req.session.username = username;
    req.session.user_id = user_id;
    console.log(req.session.user_id)

    if (!connectedUsers[req.session.id]) {
        connectedUsers[req.session.id] = username;

        res.cookie('sessionId', req.session.id, { httpOnly: true });
        res.json({ sessionId: req.session.id, message: 'Login successful' });
    } else {
        res.status(401).json({ message: 'Invalid username' });
    }
});

server.listen(3002)
<?php

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    $conn = mysqli_connect('localhost', 'root', '', 'pvi_labs');

    if(!$conn){
        http_response_code(400); 
        echo 'Connection error : ' . mysqli_connect_error();
        exit;
    }

    $sql = 'SELECT * FROM Students';

    $result = mysqli_query($conn, $sql);

    $students = mysqli_fetch_all($result, MYSQLI_ASSOC);

    echo json_encode($students, JSON_PRETTY_PRINT);

    mysqli_close($conn);
} else {
    http_response_code(405); 
    echo json_encode(array('error' => 'Method is not supported'));
}

?>

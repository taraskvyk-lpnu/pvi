const express = require('express');
const session = require('express-session');
const app = express();
const server = require('http').Server(app)
const mongoose = require('mongoose');
const io = require('socket.io')(server)
const cors = require('cors');
require('dotenv').config();

app.use(express.static('public'))
app.use(express.urlencoded({extended: true}))
app.use(express.json());

const cookieParser = require('cookie-parser');
const formidable = require('formidable');
app.use(cookieParser());

app.use(
    cors({
      origin: "http://localhost:8080",
      optionsSuccessStatus: 200,
      credentials: true,
    })
  );

app.use(
session({
    secret: 'fdiohrrhoelaeir',
    resave: false,
    saveUninitialized: true,
    cookie: {
    expires: Date.now() + 1000 * 60 * 60 * 24,
    maxAge: 1000 * 60 * 60 * 24,
    httpOnly: true,
    },
})
);

const messageSchema = new mongoose.Schema({
    content: String,
    username: String,
    userId: { type: Number, ref: 'User' },
    chat: { type: mongoose.Schema.Types.ObjectId, ref: 'Chat' },
    createdAt: { type: Date, default: Date.now }
});

const { 
    getMessageModel,
    addMessageToChat,
    createMessageModel
} = require('./server/messageController');


const { 
    addUserToDb, 
    getUserById,
    addChatToUser,
    setLastActivity
} = require('./server/userController');

const Task = require('./models/Task');
const Chat = require('./models/Chat');
const User = require('./models/User');
const DbName = 'messages';
const DB = `mongodb+srv://taraskvykpz2022:tOptBvw0zSHXtJLw@cluster0.san3ir5.mongodb.net/${DbName}?retryWrites=true&w=majority`;

app.post('/login', async (req, res) => {
    const { login, username, user_id } = req.body;
    req.session.login = login;
    req.session.username = username;
    req.session.user_id = user_id;
    console.log(req.session.user_id)

    await addUserToDb(username, user_id);
    res.cookie('sessionId', req.session.id, { httpOnly: true });
    res.cookie('user_id', user_id, { httpOnly: true });
    res.json({ user_id: user_id, message: 'Login successful' });
});

app.get('/rooms', async (req, res) => {
    if (!req.cookies || !req.cookies.user_id) {
        res.json(Object.values({}));
        return; 
    }   

    console.log(req.cookies.user_id);
    const accessibleChats = await getChatsForUser(req.cookies.user_id);
    
    console.log('chats')
    const mappedChats = accessibleChats.map(chat => ({ name: chat.name, _id: chat._id }));
    console.log(mappedChats)
    
    res.send(mappedChats);
});


app.get('/connectedUsers', async (req, res) => {
    
    const users = await User.find();
    
    console.log('users')
    console.log(users)

    res.json(users.map(user => ({ name: user.name, userId: user.userId })));
});

app.post('/room', (req, res) => {
    console.log('entered post room');

    const form = new formidable.IncomingForm();
    form.parse(req, async (err, fields, files) => {
        if (err) {
            console.error('Error parsing form data:', err);
            return res.status(500).send('Error parsing form data');
        }

        const { room: room, selectedKeys } = fields;
        
        roomName = room[0];
        const idsArr = selectedKeys[0];
        
        await createChatWithUsers(room[0], idsArr.split(','));
        res.sendStatus(200);
    });
});

app.get('/:roomId', async (req, res) => {
    const chatId = req.params.roomId;
    console.log(chatId);

    const users = await User.find({ chats: chatId });
    console.log(users);
    console.log(Object.values(users));
    return res.json({
        chatId : chatId, 
        url : 'http://localhost:8080/lab4/views/chat.html?roomName=' + chatId,
        users : users.map(user => ({ name: user.name, userId: user.userId }))
    });
})

app.use(function auth(req, res, next) {
    
    console.log("auth", req.session.id, req.url);
    next();
  });

io.on('connection', socket => {
    mongoose.connect(DB, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(async () => {    
        console.log('Connected to MongoDB');
    })
    .catch(err => console.log(err));

    socket.on('connection-with-chatId', async (chatId) => {
        
        console.log('User connected with chatId:', chatId);
        const chat = await Chat.findOne({_id: chatId });
        console.log("\t\t\t" + chat.name)
        const messageModel = await getMessageModel(chat.name, chatId, messageSchema);

        try{
            await messageModel.find({ chat: chatId }).then(result => {
                console.log(result)
                socket.emit('restored-message', result)
            })
        }catch(err){
            console.log(err)
        }
    });

    socket.on('new-user', async (chatId, user_id) => { 
        const user = await User.findOne({ userId: user_id });
        socket.join(chatId)
        socket.broadcast.to(chatId).emit('user-connected', user.name);
    });

    socket.on('send-chat-message', async (chatId, message, user_id) => {    
        
        if (chatId && socket.to(chatId)) {
            console.log('chatId:', chatId);
            console.log('message:', message);
            console.log('user_id:', user_id);
            const user = await User.findOne({ userId: user_id });
            await addMessageToChat(chatId, user_id, user.name, message);
            console.log('username:', user.name);
            
            socket.broadcast.to(chatId).emit('chat-message', { message: message, name: user.name });
        } else {    
            console.log("Error: Invalid room or socket");
        }
    })

    socket.on('addTask', async (taskData) => {
        try {
            const newTask = new Task({
                userId: taskData.userId,
                name: taskData.name,
                description: taskData.description,
                date: taskData.date,
                statusListId: taskData.statusListId
            });

            console.log('New task:', newTask);
            await newTask.save();
            io.emit('taskAdded', newTask);
        } catch (error) {
            console.error('Error adding task:', error);
        }
    });

    socket.on('getTasks', async (user_id) => {
        try {
            const tasks = await Task.find({ userId: user_id });
            io.emit('tasks', tasks);
        } catch (error) {
            console.error('Error adding task:', error);
        }
    });

    socket.on('editTaskStatus', async (taskId, listName) => {
        try {
            const task = await Task.findById(taskId);
            if(task.statusListId == listName) 
                return;

            await Task.findByIdAndUpdate(taskId, {
                statusListId: listName
            });
    
            console.log('Task status updated:', taskId, listName);
            //io.emit('taskStatusUpdated', updatedTask);
        } catch (error) {
            console.error('Error editing task status:', error);
        }
    });

    socket.on('editTask', async (updatedTaskData) => {
        try {
            // Отримання ідентифікатора завдання, яке треба змінити
            const taskId = updatedTaskData.taskId;
    
            // Оновлення даних завдання
            await Task.findByIdAndUpdate(taskId, {
                name: updatedTaskData.name,
                description: updatedTaskData.description,
                date: updatedTaskData.date,
                statusListId: updatedTaskData.statusListId
            });
    
            const updatedTask = await Task.findById(taskId);
    
            io.emit('taskUpdated', updatedTask);
        } catch (error) {
            console.error('Error editing task:', error);
        }
    });    

    socket.on('disconnect-from-room', async (previousChatId, user_id) => {   
        socket.leave(previousChatId);
        const user = await User.findOne({ userId: user_id });
        await setLastActivity(user_id);
        console.log(user.name + ' disconnected');
        socket.broadcast.to(previousChatId).emit('user-disconnected', user.name)
    })
})

app.get('/users/:userId', async (req, res) => {
    const userId = req.params.userId;
    console.log(userId);

    const chatInfos = await getChatInfoByUserId(userId);

    console.log(Object.values(chatInfos));
    res.send(Object.values(chatInfos));
})

async function getChatsForUser(user_id) {
    const user = await getUserById(user_id);
    let accessibleChats = [];

    if(user){
        const userChatsIds = user.chats;
        
        for (let chatId of userChatsIds) {
            const chat = await Chat.findOne({ _id : chatId });
    
            if(chat){
                accessibleChats.push(chat);
            }
        }
    }

    return accessibleChats;
}

async function getChatInfoByUserId(userId) {
    try {
        const user = await User.findOne({ userId: userId });
        const lastActivity = user.lastActivity;

        const chats = await Chat.find({ _id: { $in: user.chats } });
        console.log('\t\t\t\tAll chats for user ' + userId);
        console.log(chats);

        const chatInfos = [];
        for (const chat of chats) {
            const messageModel = await getMessageModel(chat.name, chat._id, messageSchema);
            console.log('\t\t\t\t\t\tMessage messageModel:', lastActivity);
            const messageCount = await messageModel.countDocuments({ chat: chat._id, createdAt: { $gt: lastActivity } });
            console.log('\t\t\t\t\t\tMessage messageCount:', messageCount);

            if (messageCount > 0) {
                chatInfos.push({
                    name: chat.name,
                    chat_id: chat._id,
                    newMessageCount: messageCount
                });
            }
        }
        console.log('\t\t\t\tChat info for user ' + userId);
        console.log(chatInfos);

        return chatInfos;
    } catch (error) {
        console.error('Помилка при отриманні інформації про чат:', error);
        throw error;
    }
}

async function createChatWithUsers(name, userIds) {
    try {
        console.log(userIds)
        const chat = new Chat({ name });
        await chat.save();
        await createMessageModel(name, chat._id);

        for (let userId of userIds) {
            
            const user = await User.findOne({ userId: userId });
            if (user) {
                addChatToUser(chat._id, userId);
                console.log(userId + " added ")
            }
        }
        await chat.save();

        console.log('Chat created with users:', chat);
        return chat._id;
    } catch (error) {
        console.error('Error creating chat with users:', error);
    }
}

server.listen(3002)
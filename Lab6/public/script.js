const socket = io('http://localhost:3002', { transports : ['websocket'] })

socket.on('chat-message', data => {
    console.log(data);
    const dataWithContent = {
        username: data.name,
        content: data.message
    };

    addAnotherMessage(dataWithContent);
})

socket.on('restored-message', dataArr => {
    const user_id = sessionStorage.getItem('user_id');
    console.log(dataArr);

    dataArr.forEach(data => {
        console.log(data);
        if(data.userId == user_id){
            restoreOwnMessage(data);
        }
        else{
            addAnotherMessage(data);
        }
    })
})

socket.on('user-connected', data => {
    addCenterMessage(data + ' joined');
})

socket.on('added-by-another-to-room', (roomName) => {
    alert("Add")
    addToRoom(roomName);
    alert("Added")
})

function addToRoom(chatName){
    fetch(`http://localhost:3002/${chatName}`, {
        method: 'GET',
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        }
        })
        .then(response => {
            if (response.status === 200) {
                document.getElementById('messageForm').removeAttribute('hidden');
                return response.json();
            } else if (response.status === 401) {
                alert('Unauthorized!\nLog in to use chat');
                document.getElementById('messageForm').setAttribute('hidden', true);
            } else {
                alert('Login failed'); 
                document.getElementById('messageForm').setAttribute('hidden', true);
            }
        })
        .then(data => {
            console.log('Response:', data);
            const users = data.users;
            const chatId = data.chatId;
            fillConnectedUsers(users);
            const li = document.querySelector(`li[data-chat-name="${chatName}"]`);

            $(".chat-item").removeClass("active");
            $(li).addClass("active");

            const user_id = sessionStorage.getItem('user_id');
            socket.emit('new-user', chatId, user_id);

            socket.emit('connection-with-chatId', chatId, user_id);

            $("#chat-messages").empty();
            addCenterMessage(`You joined ${chatName}`);
        })
        .catch(error => {
            console.error('Error fetching room:', error);
        });
}

socket.on('user-disconnected', name => {
    addCenterMessage(name + ' disconnected');
    removeChatMemberFromUI(name);
})

socket.on('room-created', roomName => {
    console.log('room created ' + roomName);
})

$("#messageForm").submit(function(event) {
    event.preventDefault();
    sendOwnMessage();    
});

function sendOwnMessage(){
    var message = $("#messageInput").val();

    if (message.trim() !== "") {
      var ownMessage = `
      <div class="own-message mb-2">
            <div class="card-no-border">
                <div class="card-body text-right">
                    <div class="border border-secondary p-2 rounded d-inline-block">
                        <div class="d-flex align-items-center justify-content-end">
                            <i class="bi bi-person" style="color: rgb(200, 196, 196);"></i>
                            <p class="mb-0 font-weight-bold" style="color: rgb(200, 196, 196); font-size: 13px;">You</p>
                        </div>
                        <p class="card-text">${message}</p>
                    </div>
                </div>
            </div>
        </div>
        `;
       
        $("#chat-messages").append(ownMessage);
        const user_id = sessionStorage.getItem('user_id');
        const chatId = $("#chatId").val();
        
        socket.emit('send-chat-message', chatId, message, user_id)
        $("#messageInput").val("");
        scrollToBottom();
    }
}

function restoreOwnMessage(data){
    var ownMessage = `
    <div class="own-message mb-2">
          <div class="card-no-border">
              <div class="card-body text-right">
                  <div class="border border-secondary p-2 rounded d-inline-block">
                      <div class="d-flex align-items-center justify-content-end">
                          <i class="bi bi-person" style="color: rgb(200, 196, 196);"></i>
                          <p class="mb-0 font-weight-bold" style="color: rgb(200, 196, 196); font-size: 13px;">You</p>
                      </div>
                      <p class="card-text">${data.content}</p>
                  </div>
              </div>
          </div>
      </div>
      `;

      $("#chat-messages").append(ownMessage);
      scrollToBottom();
}

function addAnotherMessage(data){
    var anotherMessage = `
        <div class="other-message mb-2">
            <!-- Other user's message -->
            <div class="card-no-border m-0">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <img src="/lab4/images/placeholder50.png" style="width: 50px; height: 50px;" class="rounded-circle mr-2 user-img" alt="User 2">
                        <div class="border border-secondary p-2 rounded">
                            <div class="d-flex align-items-center justify-content-start">
                                <i class="bi bi-person" style="color: rgb(200, 196, 196);"></i>
                                <p class="mb-0 font-weight-bold" style="color: rgb(200, 196, 196); font-size: 13px;">${data.username}</p>
                            </div>
                            <p class="mb-0">${data.content}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
      $("#chat-messages").append(anotherMessage);
      scrollToBottom();
}

function addCenterMessage(message) {
    var centerMessage = `
    <div class="mb-2">
        <!-- Other user's message -->
        <div class="card-no-border m-0">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-center">
                    <div class="d-flex align-text-center border border-secondary p-1 rounded">
                        <p class="mb-0">${message}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `;

    $("#chat-messages").append(centerMessage);
    scrollToBottom();
}

function scrollToBottom() {
    var chatBody = $("#chat-messages");
    chatBody.scrollTop(chatBody.prop("scrollHeight"));
}

async function loadChatList(){
    await fetch('http://localhost:3002/rooms', {
        method: "GET",
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response =>{
            return response.json()
        } )
        .then(function(rooms) {
            console.log("Rooms : " + rooms);
            $('#chat-list').empty();
            rooms.forEach(room => {
                const li = `
                <li class="list-group-item chat-item" data-chat-name="${room.name}" data-username="${room.name}" data-chat-id="${room._id}" onclick="setupChatItemClickHandler(this)">
                    <input type="hidden" name="chatId" value="${room._id}">
                    <img src="/lab4/images/placeholder50.png" style="width: 50px; height: 50px;" class="img-fluid rounded-circle mr-2" alt="${room.name}"> ${room.name}
                </li>
                `;
                $('#chat-list').append(li);
            });
        })
        .catch(error => console.error('Error fetching rooms:', error));
}

async function loadNewMessages(){
    
    const user_id = sessionStorage.getItem('user_id');

    await fetch(`http://localhost:3002/users/${user_id}`, {
        method: 'GET',
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        }})
        .then(response => {
            if (response.ok) {
              return response.json();
            }
            throw new Error('Network response was not ok.');
          })
          .then(chatInfos => {
            console.log('New messages in ' + chatInfos.length + ' chats');
            console.log(chatInfos);
            
            if(chatInfos === undefined || chatInfos.length === 0){
                $("#bell-circle").hide();
            }
            else{
                chatInfos.forEach(async (chatInfo) => {
                    await appendNewMessageToDropdownMenu(chatInfo);
                }
                );
                $("#bell-circle").show();
            }
          })
          .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
          });
}


async function appendNewMessageToDropdownMenu(chatInfo){
    const chatName = chatInfo.name;
    const chatId = chatInfo.chat_id;
    const messageCount = chatInfo.newMessageCount;
    const li = `
    <li class="dropdown-item" data-chat-name="${chatName}" data-chat-id="${chatId}">
        <svg style="width: 25px; height: 25px; color: black; padding-right: 5px;" xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0"/>
            <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8m8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1"/>
        </svg>
        <a class="message-link" href="/lab4/views/chat.html?chat-id=${chatId}"><span style="color: black;">${chatName} ${messageCount}</span></a>
    </li>
    `;

    $('.new-messages-dropdown').append(li);
}
async function setupChatItemClickHandler(li){
    var chatName = $(li).data("chat-name");
    var chatId = $(li).data("chat-id");
    console.log("Chat ID : " + chatId);
    var previousChatId = $("#chatId").val();
    var previousRoomName = $("#roomName").text();

    if(chatName === previousRoomName){
        return;
    }
    
    $("#roomName").text(chatName);
    $(".own-message .font-weight-bold").text("You");
    await loadChat(chatId, previousChatId);
    $(li).addClass("active");
}

async function loadChat(chatId, previousChatId = 0) {
    // Виконання запиту для маршруту /:room
    await fetch(`http://localhost:3002/${chatId}`, {
        method: 'GET',
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        }})
        .then(response => {
            if (response.status === 200) {
                document.getElementById('messageForm')?.removeAttribute('hidden');

                var chatIdInput = document.getElementById("chatId");

                if(chatIdInput != null){
                    chatIdInput.value = chatId;
                }
                return response.json();
            } else if (response.status === 401) {
                alert('Unauthorized!\nLog in to use chat');
                document.getElementById('messageForm').setAttribute('hidden', true);
            } else {
                alert('Login failed'); 
                document.getElementById('messageForm').setAttribute('hidden', true);
            }
        })
        .then(async (data) => {
            console.log('Response:', data);

            document.getElementById('chat-messages').setAttribute('hidden', 'true');
            const user_id = sessionStorage.getItem('user_id');
            const users = data.users;
            console.log("users : " + users);
            fillConnectedUsers(users);
            socket.emit('disconnect-from-room', previousChatId, user_id);
            
            $(".chat-item").removeClass("active");
            
            await socket.emit('connection-with-chatId', chatId, user_id);
            await socket.emit('new-user', chatId, user_id);

            const taskData = {
                userId : 0,
                name: "name",
                description: "description",
                date: 0,
                statusListId: "toDo"
              }
            
            socket.emit('addTask', taskData);
            $("#chat-messages").empty();
            document.getElementById('chat-messages').removeAttribute('hidden');
            //addCenterMessage(`You joined ${chatName}`);
            //addChatMemberToUI("You");
        })
        .catch(error => {
            console.error('Error fetching room:', error);
        });
}

async function getConnectedUsers() {              
        $.ajax({
        url: "http://localhost:3002/connectedUsers",
        type: 'GET',
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        success: function(data) {
            $("#connectedUsersDropdown").empty();
            
            var allKeys = Object.keys(data);
            const user_id = sessionStorage.getItem('user_id');
            
            allKeys.forEach(function(key) {
                if(user_id != data[key].userId) {
                    var label = document.createElement("label");
                
                    var checkbox = document.createElement("input");
                    checkbox.type = "checkbox";
                    checkbox.value = data[key].userId;
                    label.appendChild(checkbox);
                    
                    console.log(data[key].userId);

                    label.appendChild(document.createTextNode(data[key].name));
                    label.className = "dropdown-item";
                    
                    $("#connectedUsersDropdown").append(label);   
                }
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.error('Error fetching connected users:', textStatus, errorThrown);
        }
    });
}

function fillConnectedUsers(users) {
    const chatHeader = document.getElementById('chat-header');

    chatHeader.innerHTML = '';

    users.forEach(user => {
        addChatMemberToUI(user.name);
    });
}

function addChatMemberToUI(name){
    var userElement = 
    `
    <div class="d-inline-block text-center m-0 connected-user" style="max-height: 30px;">
        <i class="bi bi-person" style="color: rgb(0, 0, 0);"></i>
        <p class="mt-0 ml-1 text-center font-size-small">${name}</p>
    </div>
    `;

    const chatHeader = document.getElementById('chat-header');
    chatHeader.innerHTML +=userElement;
}

function removeChatMemberFromUI(name) {
    const chatHeader = document.getElementById('chat-header');
    const userElements = chatHeader.getElementsByClassName('connected-user');

    for (let i = 0; i < userElements.length; i++) {
        const userNameElement = userElements[i].querySelector('.font-size-small');
        if (userNameElement.textContent == name) {
            const userElement = userElements[i];
            userElement.parentNode.removeChild(userElement);
            break; 
        }
    }
}

async function authenticateUser(login, password) {
    let userName = '';
    await $.ajax({
        url: 'http://localhost:8080/lab4/php/signIn.php',
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({ login: login, password: password }),
        success: async function(data) {
            if (data.success) {
                userName = data.name; 
                localStorage.setItem(login, userName);
                sessionStorage.setItem('user_id', data.user_id);
                sessionStorage.setItem('userName', userName);

                $('#login-link').text('Log out');
                $('#user-name').text(userName);
                $('#messageForm').show();
                document.getElementById('addRoomForm')?.removeAttribute('hidden');
                console.log('User authenticated successfully');
                await loadChatList();
            } else {
                alert('Invalid username or password');
                console.log('Invalid username or password');
            }
        },
        error: function(xhr, status, error) {
            //$('#user-name').text(login);
            const response = JSON.parse(xhr.responseText);
        
           // alert(response.error);
            console.error('AJAX request failed:', error);
        }
    });

    const user_id = sessionStorage.getItem('user_id');
    
    fetch('http://localhost:3002/login', {
            method: 'POST',
            credentials: "include",
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({ login: login, username: userName, user_id : user_id})
        })
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                console.error('Login failed');
            }
        })
        .then(data => {
            if (data.user_id) {
                // Store session ID in sessionStorage
                sessionStorage.setItem('user_id', data.user_id);
                console.log('User ID stored in sessionStorage:', data.user_id);
            } else {
                console.error('User ID not found in response');
            }
        })
        .catch(error => console.error('Error:', error));
}
//const socket1 = io('http://localhost:3002', { transports : ['websocket'] })
var nexElemId = 9;

function loadTasks(userId){
  socket.emit('getTasks', userId);
  socket.on('tasks', tasks => {
    console.log(tasks);
    tasks.forEach(task => {
      appendTaskToUI(task);
    })
  });
}

function addTask(status) {
  document.getElementById('upsertTaskName').value = '';
  document.getElementById('upsertTaskDescription').value = '';
  document.getElementById('upsertTaskDate').value = '';
  document.getElementById('upsertTaskStatus').value = status;
  
  console.log(document.getElementById('upsertTaskStatus').value)
  $('#upsertTaskModal').find('.modal-title').text('Add Task');
  document.getElementById('isEdit').value = '0';
  document.getElementById('upsertTaskId').value = '0';

  $('.upsertTaskStatus-control').prop('hidden', true); 
  $('#upsertTaskModal').modal('show');
}

function editTask(button) {
  $('#upsertTaskModal').find('.modal-title').text('Edit Task');
  document.getElementById('isEdit').value = '1';
  $('.upsertTaskStatus-control').prop('hidden', true);

  $('#upsertTaskModal').modal('show');
  var taskItem = button.closest('.list-group-item');
  var parentId = taskItem.parentNode.parentNode.id;
  document.getElementById('upsertTaskStatus').value = `${parentId}`;
  
  // Отримуємо дані з картки
  var taskName = taskItem.querySelector('.task-name').textContent.trim();
  var taskDescription = taskItem.querySelector('textarea').value;
  console.log(taskItem.querySelector('textarea'))
  var taskDate = taskItem.querySelector('input[type="date"]').value;
  var taskId = taskItem.querySelector('.Id').value;

  // Встановлюємо значення відповідним елементам форми редагування
  document.getElementById('upsertTaskName').value = taskName;
  document.getElementById('upsertTaskDescription').value = taskDescription;
  document.getElementById('upsertTaskDate').value = taskDate;
  document.getElementById('upsertTaskId').value = taskId;
}

function saveChanges() {
  var name = document.getElementById('upsertTaskName').value;
  var description = document.getElementById('upsertTaskDescription').value;
  var date = document.getElementById('upsertTaskDate').value;
  var statusListId = document.getElementById('upsertTaskStatus').value;
  var isEdit = document.getElementById('isEdit').value;
  var taskId = document.getElementById('upsertTaskId').value;

  if(isEdit == 0){
    
    const user_id = sessionStorage.getItem('user_id');
    const taskData = {
      userId: user_id,
      name: name,
      description: description,
      date: date,
      statusListId: statusListId
    }
    
      socket.emit('addTask', taskData);
      socket.on('taskAdded', taskData => {
      console.log(taskData);
    
      appendTaskToUI(taskData);
    })
  }
  else {
    var taskToEdit = getLiElementByTaskId(taskId);

    const updatedTaskData = {
        taskId: taskId,
        name: name,
        description: description,
        date: date,
        statusListId: statusListId
    };

    socket.emit('editTask', updatedTaskData);
    
    taskToEdit.querySelector('.task-name').textContent = name;
    taskToEdit.querySelector('textarea').value = description;
    taskToEdit.querySelector('input[type="date"]').value = date;
  }

  // Close modal
  $('#upsertTaskModal').modal('hide');
  document.getElementById('upsertTaskName').value = '';
  document.getElementById('upsertTaskDescription').value = '';
  document.getElementById('upsertTaskDate').value = '';
}

async function updateListName(taskId, listName) {
  await socket.emit('editTaskStatus', taskId, listName);
}

function appendTaskToUI(taskData){
    var newTask = createNewTask(taskData);
    var taskList = document.querySelector(`#${taskData.statusListId}`);
    var ul = taskList.querySelector('ul');
    ul.appendChild(newTask);
}

function createNewTask(taskData) {
    // Створення нового елемента <li>
    var newLi = document.createElement('li');
    newLi.classList.add('list-group-item', 'draggable-item', 'm-1');

    // Створення елемента <input> з класом 'Id'
    var inputId = document.createElement('input');
    inputId.setAttribute('type', 'hidden');
    inputId.classList.add('Id');
    inputId.value = taskData._id; // Припустимо, що id завдання встановлюється як '8'

    // Створення елемента <span> з класом 'task-name'
    var spanTaskName = document.createElement('span');
    spanTaskName.classList.add('task-name');
    spanTaskName.textContent = taskData.name;

    // Створення кнопки 'Edit'
    var buttonEdit = document.createElement('button');
    buttonEdit.classList.add('btn', 'btn-sm', 'btn-primary', 'float-right');
    buttonEdit.textContent = 'Edit';
    buttonEdit.onclick = function() {
      editTask(this);
    };

    // Створення контейнера для опису та дати
    var divDescriptionDate = document.createElement('div');
    divDescriptionDate.classList.add('mt-2');

    // Створення текстової області для опису
    var textareaDescription = document.createElement('textarea');
    textareaDescription.classList.add('form-control', 'mb-2');
    textareaDescription.setAttribute('placeholder', 'Description');
    textareaDescription.setAttribute('readonly', 'readonly');
    textareaDescription.textContent = taskData.description;

    const taskDate = new Date(taskData.date);
    // Форматування дати у формат YYYY-MM-DD
    const formattedDate = taskDate.toISOString().split('T')[0];
    // Створення елемента <input> для дати
    var inputDate = document.createElement('input');
    inputDate.setAttribute('type', 'date');
    inputDate.setAttribute('readonly', 'readonly');
    inputDate.classList.add('form-control');
    inputDate.setAttribute('placeholder', 'Date');
    inputDate.value = formattedDate;

    // Додавання елементів до контейнера опису та дати
    divDescriptionDate.appendChild(textareaDescription);
    divDescriptionDate.appendChild(inputDate);

    // Додавання всіх створених елементів до нового <li>
    newLi.appendChild(inputId);
    newLi.appendChild(spanTaskName);
    newLi.appendChild(buttonEdit);
    newLi.appendChild(divDescriptionDate);

    // Повертаємо створений <li>
    return newLi;
}

function getLiElementByTaskId(taskId) {
  // Отримуємо всі елементи <input> з класом 'Id'
  var inputs = document.querySelectorAll('.Id');

  // Перебираємо кожний елемент <input>
  for (var i = 0; i < inputs.length; i++) {
      var input = inputs[i];

      // Перевіряємо, чи значення поточного елементу <input> співпадає з taskId
      if (input.value === taskId) {
          // Якщо так, отримуємо його батьківський елемент <li> і повертаємо його
          return input.closest('li');
      }
  }

  // Якщо немає збігів, повертаємо null
  return null;
}
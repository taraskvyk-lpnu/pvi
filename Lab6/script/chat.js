
function getUserNameFromLocalStorage(){
    var username = localStorage.getItem('username');

    if (username) {
        return username;
    } 
    
    return 'Anonymous';
}
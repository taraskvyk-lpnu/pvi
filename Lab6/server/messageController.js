const mongoose = require('mongoose');
const Chat = require('../models/Chat');

const messageSchema = new mongoose.Schema({
    content: String,
    username: String,
    userId: { type: Number, ref: 'User' },
    chat: { type: mongoose.Schema.Types.ObjectId, ref: 'Chat' },
    createdAt: { type: Date, default: Date.now }
});

async function addMessageToChat(chatId, userId, username, messageContent) {
    try {
        // Знайти чат за його id
        console.log(chatId)
        const chat = await Chat.findById(chatId);
        if (!chat) {
            console.log('Chat not found');
            return;
        }

        const messageModel = await getMessageModel(chat.name, chatId, messageSchema);
        const message = new messageModel({ 
            content: messageContent, 
            username: username,
            chat: chat._id,
            userId: userId
        
        });
        await message.save();

        chat.messages.push(message);
        await chat.save();
        console.log('Message added to chat:', message);
    } catch (error) {
        console.error('Error adding message to chat:', error);
    }
}

function createMessageModel(chatName, chatId) {
    const modelName = `${chatName.replace(/\s/g, '')}_${chatId}_Messages`;
    return mongoose.model(modelName, messageSchema);
}

async function getMessageModel(chatName, chatId, messageSchema) {
    const modelName = `${chatName.replace(/\s/g, '')}_${chatId}_messages`;

    if (mongoose.models[modelName]) {
        return mongoose.models[modelName];
    }

    return mongoose.model(modelName, messageSchema);
}

module.exports = {
    getMessageModel,
    addMessageToChat,
    createMessageModel
};
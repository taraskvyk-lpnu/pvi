const User = require('../models/User');
const ObjectId = require('mongodb').ObjectId; 

async function addUserToDb(name, userId, chatId = -1) {
    try {
        const existingUser = await User.findOne({ userId: userId });

        if (existingUser) {
            console.log('Користувач з таким userId вже існує:', existingUser);
            return existingUser;
        }

        let newUser;
        
        if (chatId === -1) {
            newUser = new User({
                name: name,
                userId: userId,
                chats: []
            });
        }
        else{
            console.log(userId)
            newUser = new User({
                name: name,
                userId: userId,
                chats: [ObjectId(chatId)]
            });
        }

        const savedUser = await newUser.save();
        console.log('Користувач успішно доданий:', savedUser);
        return savedUser;
    } catch (error) {
        console.error('Помилка під час додавання користувача:', error);
        throw error;
    }
}

async function getUserById(userId) {
    try {
        const user = await User.findOne({ userId: userId });

        if (!user) {
            console.log('Користувача не знайдено');
            return null;
        }

        console.log('Користувача знайдено:', user);
        return user;
    } catch (error) {
        console.error('Помилка під час отримання користувача:', error);
        throw error;
    }
}

async function setLastActivity(userId) {
    try {
        const user = await User.findOne({ userId: userId });

        if (!user) {
            console.log('Користувача не знайдено');
            return;
        }

        user.lastActivity = new Date();
        
        await user.save();

        console.log('Час останньої активності оновлено для користувача', userId, ':', user.lastActivity);
    } catch (error) {
        console.error('Помилка при встановленні часу останньої активності:', error);
        throw error;
    }
}

async function addChatToUser(chatId, userId) {
    try {
        const user = await User.findOne({ userId: userId });

        if (!user) {
            console.error('Користувач не знайдений!');
            return;
        }

        if (user.chats.includes(chatId)) {
            console.log('Чат вже додано до списку чатів користувача.');
            return;
        }

        user.chats.push(chatId);
        await user.save();

        console.log('Чат успішно додано до списку чатів користувача.');
    } catch (error) {
        console.error('Помилка під час додавання чату до списку чатів користувача:', error);
    }
}

module.exports = {
    addUserToDb,
    getUserById,
    setLastActivity,
    addChatToUser
};
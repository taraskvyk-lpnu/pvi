// utils.js
function myFunction1() {
    console.log("Function 1 executed");
}

function myFunction2(param) {
    console.log("Function 2 executed with parameter:", param);
}

// Експортуємо функції
module.exports = {
    myFunction1,
    myFunction2
};
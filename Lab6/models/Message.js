// Підключення необхідних бібліотек
const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
    content: String,
    username: String,
    userId: { type: Number, ref: 'User' },
    chat: { type: mongoose.Schema.Types.ObjectId, ref: 'Chat' },
    createdAt: { type: Date, default: Date.now }
});

const Message = mongoose.model('Message', messageSchema);

module.exports = Message;
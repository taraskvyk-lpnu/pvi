const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
    name: String,
    userId : Number,
    chats: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Chat' }],
    lastActivity: Date
});

const User = mongoose.model('User', userSchema);

module.exports = User;
const CACHE_NAME = 'my-site-cache-v1';

const urlsToCache = [
  '/Code/index.html',
  '/Code/styles/styles.css',
  '/Code/images/student-icon.png',
  '/Code/script/animation.js',
  '/Code/script/script-jQuery.js',
  '/Code/script/validation.js'
];

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        if (response) {
          return response;
        }

        return fetch(event.request);
      })
  );
});
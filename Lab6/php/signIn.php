<?php 

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    
    $data = json_decode(file_get_contents('php://input'), true);

    $conn = mysqli_connect('localhost', 'root', '', 'pvi_labs');

    if(!$conn){
        echo 'Connection error : ' . mysqli_connect_error();
        exit;
    }

    $login = $data['login'];
    $password = $data['password'];
    
    $sql = "SELECT * FROM Users WHERE login = '$login' AND password = '$password'";

    $result = mysqli_query($conn, $sql);
    
    if(!$result){
        http_response_code(400); 
        echo json_encode(array("success" => false, "error" => "Such user does not exist."), JSON_PRETTY_PRINT);
        exit;
    }

    if(mysqli_num_rows($result) > 0){
        // Отримуємо дані першого студента
        $student = mysqli_fetch_assoc($result);

        // Отримуємо ім'я та Id студента
        $name = $student['Name'];
        $userId = $student['id'];

        http_response_code(200);
        echo json_encode(array("success" => true, "user_id" => $userId, "student" => $login, "name" => $name), JSON_PRETTY_PRINT);
    } else {
        http_response_code(400); 
        echo json_encode(array("success" => false, "error" => "Such user does not exist."), JSON_PRETTY_PRINT);
    }
    
} else {
    http_response_code(405);
    echo json_encode(array("success" => false, "error" => "Invalid request method."), JSON_PRETTY_PRINT);
}
?>
<?php
if ($_SERVER["REQUEST_METHOD"] === "DELETE") {

    $studentId = $_GET['id'] ?? null;

    if ($studentId === null) {
        http_response_code(400); 
        echo json_encode(array("success" => false, "error" => "Id is empty."), JSON_PRETTY_PRINT);
        exit; 
    }

    $conn = mysqli_connect('localhost', 'root', '', 'pvi_labs');

    if(!$conn){
        echo 'Connection error : ' . mysqli_connect_error();
    }

    $escaped_id = mysqli_real_escape_string($conn, $studentId);
    $sql = "DELETE FROM Students WHERE student_id = '$escaped_id'";

    $result = mysqli_query($conn, $sql);

    $deleted = mysqli_affected_rows($conn) > 0;

    mysqli_close($conn);

    if ($deleted) {
        http_response_code(200);
        echo json_encode(array("success" => true), JSON_PRETTY_PRINT);
    } else {
        http_response_code(404);
        echo json_encode(array("success" => false, "error" => "Student is not found."), JSON_PRETTY_PRINT);
    }
} else {
    http_response_code(405);
    echo json_encode(array("success" => false, "error" => "Not valid Method. DELETE requset expects."), JSON_PRETTY_PRINT);
}
?>

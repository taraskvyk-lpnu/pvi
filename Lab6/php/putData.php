<?php
function isValidData($studentData) {
    $isValid = true;

    if (!isValidGroup($studentData['group_name'])) {
        $isValid = false;
    }

    if (!isValidName($studentData['name'], false)) {
        $isValid = false;
    }
    
    if (!isValidName($studentData['surname'], true)) {
        $isValid = false;
    
    }
    if (!isValidGender($studentData['gender'])) {
        $isValid = false;
    }

    if (!isValidDate($studentData['birthdate'])) {
        $isValid = false;
    }

    return $isValid;
}

function isValidGroup($group) {
    if (!$group) {
        $response = [
            'success' => false,
            'message' => 'Choose group!'
        ];
        echo json_encode($response);
        return false;
    } else {
        return true;
    }
}

function isValidGender($gender) {
    if (!$gender) {
        $response = [
            'success' => false,
            'message' => 'Choose gender!'
        ];
        echo json_encode($response);
        return false;
    } else {
        return true;
    }
}

function isValidName($name, $isSurname) {
    $fieldName = $isSurname ? 'Surname' : 'Name';

    if (!$name) {
        $response = [
            'success' => false,
            'message' => "{$fieldName} field cannot be empty!"
        ];
        echo json_encode($response);
        return false;
    }

    if (strlen($name) < 2) {
        $response = [
            'success' => false,
            'message' => "{$fieldName} should contain at least 2 letters!"
        ];
        echo json_encode($response);
        return false;
    }

    $firstLetter = $name[0];

    if (!preg_match('/^[A-ZА-Я]/u', $firstLetter)) {
        $response = [
            'success' => false,
            'message' => "{$fieldName} should start with a capital letter!"
        ];
        echo json_encode($response);
        return false;
    } else if (!preg_match('/^[a-zA-Zа-яА-Я]+$/', $name)) {
        $response = [
            'success' => false,
            'message' => "{$fieldName} should contain only letters!"
        ];
        echo json_encode($response);
        return false;
    } else {
        return true;
    }
}

function isValidDate($birthDate) {
    if (!$birthDate) {
        $response = [
            'success' => false,
            'message' => 'Birthdate field cannot be empty!'
        ];
        echo json_encode($response);
        return false;
    }

    $parts = explode('-', $birthDate);
    $year = intval($parts[0]);

    $currentYear = intval(date('Y'));
    if ($year < 1990 || $year > 2006 || $year > $currentYear) {
        $response = [
            'success' => false,
            'message' => 'Birthdate date should be from 1990 to 2006!'
        ];
        echo json_encode($response);
        return false;
    } else {
        return true;
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    $data = json_decode(file_get_contents('php://input'), true);

    $errors = array();

    if(isValidData($data)) {
        $id = $_GET['id'];
        
        $conn = mysqli_connect('localhost', 'root', '', 'pvi_labs');

         if(!$conn){
             echo 'Connection error : ' . mysqli_connect_error();
             exit;
         }
         
         $name = $data['name'];
         $surname = $data['surname'];
         $group_name = $data['group_name'];
         $gender = $data['gender'];
         $status = false;
         $birthdate = $data['birthdate'];
         
         $sql = "UPDATE Students SET 
         name = '$name',
         surname = '$surname',
         group_name = '$group_name',
         gender = '$gender',
         status = '$status',
         birthdate = '$birthdate'
         WHERE student_id = '$id'";
 
         $result = mysqli_query($conn, $sql);
         
        if ($result) {
            mysqli_close($conn);

            http_response_code(200);
            echo json_encode(array("success" => true, "id" => $id), JSON_PRETTY_PRINT);
        } else {
            http_response_code(500); 
            echo json_encode(array("success" => false, "error" => "Could not found element with id '$id'"), JSON_PRETTY_PRINT);
        }
    } else {
        http_response_code(400);
        echo json_encode(array("success" => false, "errors" => $errors), JSON_PRETTY_PRINT);
    }
} else {
    http_response_code(405);
    echo json_encode(array("success" => false, "error" => "Invalid request method."), JSON_PRETTY_PRINT);
}

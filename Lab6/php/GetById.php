<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $id = $_GET['id'];

    $conn = mysqli_connect('localhost', 'root', '', 'pvi_labs');

    if(!$conn){
        echo 'Connection error : ' . mysqli_connect_error();
    }

    $escaped_id = mysqli_real_escape_string($conn, $id);

    $sql = "SELECT * FROM Students WHERE student_id = '$escaped_id'";

    $result = mysqli_query($conn, $sql);

    $student = mysqli_fetch_assoc($result);

    if ($student !== null) {
        http_response_code(200);
        echo json_encode($student, JSON_PRETTY_PRINT);
    } else {
        http_response_code(404);
        echo json_encode(array("error" => "Student is not found with id = '$escaped_id'."), JSON_PRETTY_PRINT);
    }

    mysqli_close($conn);
} else {
    http_response_code(405);
    echo json_encode(array("error" => "Not valid Method. DELETE requset expects."), JSON_PRETTY_PRINT);
}

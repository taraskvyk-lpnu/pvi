<?php

$responses = [];

function isValidData($studentData) {
    global $responses;
    $isValid = true;

    if (!isValidGroup($studentData['group_name'])) {
        $isValid = false;
    }

    if (!isValidName($studentData['name'], false)) {
        $isValid = false;
    }
    
    if (!isValidName($studentData['surname'], true)) {
        $isValid = false;
    
    }
    if (!isValidGender($studentData['gender'])) {
        $isValid = false;
    }

    if (!isValidDate($studentData['birthdate'])) {
        $isValid = false;
    }

    return $isValid;
}

function isValidGroup($group) {
    global $responses;
    if (!$group) {
        $responses[] = [
            'success' => false,
            'message' => 'Choose group!'
        ];
        return false;
    } else {
        return true;
    }
}

function isValidGender($gender) {
    global $responses;
    if (!$gender) {
        $responses[] = [
            'success' => false,
            'message' => 'Choose gender!'
        ];
        return false;
    } else {
        return true;
    }
}

function isValidName($name, $isSurname) {
    global $responses;
    $fieldName = $isSurname ? 'Surname' : 'Name';

    if (!$name) {
        $responses[] = [
            'success' => false,
            'message' => "{$fieldName} field cannot be empty!"
        ];
        return false;
    }

    if (strlen($name) < 2) {
        $responses[] = [
            'success' => false,
            'message' => "{$fieldName} should contain at least 2 letters!"
        ];
        return false;
    }

    $firstLetter = $name[0];

    if (!preg_match('/^[A-ZА-Я]/u', $firstLetter)) {
        $responses[] = [
            'success' => false,
            'message' => "{$fieldName} should start with a capital letter!"
        ];
        return false;
    } else if (!preg_match('/^[a-zA-Zа-яА-Я]+$/', $name)) {
        $responses[] = [
            'success' => false,
            'message' => "{$fieldName} should contain only letters!"
        ];
        return false;
    } else {
        return true;
    }
}

function isValidDate($birthDate) {
    global $responses;
    if (!$birthDate) {
        $responses[] = [
            'success' => false,
            'message' => 'Birthday field cannot be empty!'
        ];
        return false;
    }

    $parts = explode('-', $birthDate);
    $year = intval($parts[0]);

    $currentYear = intval(date('Y'));
    if ($year < 1990 || $year > 2006 || $year > $currentYear) {
        $responses[] = [
            'success' => false,
            'message' => 'Birthday date should be from 1990 to 2006!'
        ];
        return false;
    } else {
        return true;
    }
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    
    $data = json_decode(file_get_contents('php://input'), true);

    if(isValidData($data)) {
        
         $conn = mysqli_connect('localhost', 'taras', 'taras2201', 'pvi_labs');

         if(!$conn){
             echo 'Connection error : ' . mysqli_connect_error();
             exit;
         }
 
         $name = $data['name'];
         $surname = $data['surname'];
         $group_name = $data['group_name']; 
         $gender = $data['gender'];
         $status = false;
         $birthdate = $data['birthdate'];
         
         $sql = "INSERT INTO Students VALUES ('', '$name', '$surname', '$group_name', '$gender', '$status', '$birthdate')";
 
         $result = mysqli_query($conn, $sql);
        if ($result) {
            $newId = mysqli_insert_id($conn);
            mysqli_close($conn);

            http_response_code(200);
            echo json_encode(array("success" => true, "id" => $newId), JSON_PRETTY_PRINT);
        } else {
            http_response_code(500);
            echo json_encode(array("success" => false, "error" => "Insertion error."), JSON_PRETTY_PRINT);
        }
    } 

    if (!empty($responses)) {
        http_response_code(400);
        echo json_encode($responses, JSON_PRETTY_PRINT);
    }
    
} else {
    http_response_code(405);
    echo json_encode(array("success" => false, "error" => "Invalid request method."), JSON_PRETTY_PRINT);
}

function disableOverlay()
{
    var overlay = $("#overlay");
    overlay.css("display", "none");
}

function enableOverlay()
{
    var overlay = $("#overlay");
    overlay.css("display", "block");
}

function hideWarningWindow()
{
    $('#warning-container').css("display", "none");
    disableOverlay();
}

function processAllCheckboxes()
{
    var mainCheckbox = $('#main-checkbox');
    var checkboxes = $('table input[type="checkbox"]');

    if(mainCheckbox.prop("checked")) {
        checkboxes.prop("checked", true);
    }
    else {
        checkboxes.prop("checked", false);
    }
}

function bellBtnClicked() {
    var dropdown = $('#notification-dropdown');
    dropdown.css("display", "block");

    var redCircle = $('#bell-circle');
    redCircle.css("display", "none");
}

function showNotification() {
    hideUserMenu();
    var addRowBtn = $('#addRow');
    addRowBtn.css("position", "relative");

    var dropdown = $('#notification-dropdown');
    dropdown.css("display", "block");
}

function hideNotifications() {
    var dropdown = $('#notification-dropdown');
    dropdown.css("display", "none");
    
    var addRowBtn = $('#addRow');
    addRowBtn.css("position", "static");
}

function showUserMenu() {
    hideNotifications();
    var addRowBtn = $('#addRow');
    addRowBtn.css("position", "relative");
    
    var dropdown = $('#user-menu-dropdown');
    dropdown.css("display", "block");
}

function hideUserMenu() {
    var dropdown = $('#user-menu-dropdown');
    dropdown.css("display", "none");

    var addRowBtn = $('#addRow');
    addRowBtn.css("position", "static");
}

function closeInputForm() {
    setupAddForm();
    var centeredDiv = $("#form-container");
    centeredDiv.css("display", "none");
    $('.errorSpan').hide();
    disableOverlay();
}

function getIndex(row) {
    var table = $(row).parent();
    var index = 0;

    while ((row = $(row).prev()).length) {
        index++;
    }

    return index;
}

function toggleLogin() {
    var loginLink = $('#login-link');
    var userName = $('#user-name');
}

function showWarningWindow(row) {
    var warningContainer = $('#warning-container');
    warningContainer.css("display", "block");

    var warningMessage = $('#warning-message');
    var studentName = ''; 

    if (row) {
        var cells = $(row).find('td');
        if (cells.length >= 3) { 
            studentName = cells.eq(3).text();
        }
    }

    warningMessage.html('<p> Are you really want to delete ' + studentName + '?</p>');
    warningContainer.data("rowIndex", getIndex(row)); 
}
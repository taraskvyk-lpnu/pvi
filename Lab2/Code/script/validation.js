function isValidData(studentData) {

    var isValid = true;

    if(!isValidGroup(studentData.group)){
        isValid = false;
    }
    
    if(!isValidGender(studentData.gender)){
        isValid = false;
    }

    if(!isValidName(studentData.name, false))
        isValid = false;

    if(!isValidName(studentData.surname, true))
        isValid = false;

    if (!isValidDate(studentData.birthday)) {
        isValid = false;
    }

    return isValid;
}

function isValidGroup(group) {
    if(!group){
        $(`#group-error`).css('display', 'block');
        $(`#group-error`).text(`Choose group!`);
        isValid = false;
    }
    else
    {
        $(`#group-error`).text('');
        return true;
    }
}

function isValidGender(gender) {
    if(!gender){
        $(`#gender-error`).css('display', 'block');
        $(`#gender-error`).text(`Choose gender!`);
        isValid = false;
    }
    else
    {
        $(`#gender-error`).text('');
        return true;
    }
}

function isValidName(name, isSurname) {
    const fieldName = isSurname ? 'Surname' : 'Name';

    if (!name) {
        $(`#${isSurname ? 'surname' : 'name'}-error`).css('display', 'block');
        $(`#${isSurname ? 'surname' : 'name'}-error`).text(`${fieldName} field cannot be empty!`);
        return false;
    }

    if (name.length < 2) {
        $(`#${isSurname ? 'surname' : 'name'}-error`).css('display', 'block');
        $(`#${isSurname ? 'surname' : 'name'}-error`).text(`${fieldName} should contain at least 2 letters!`);
        return false;
    }

    const firstLetter = name.charAt(0);

    if (!/^[A-ZА-Я]/.test(firstLetter)) {
        $(`#${isSurname ? 'surname' : 'name'}-error`).css('display', 'block');
        $(`#${isSurname ? 'surname' : 'name'}-error`).text(`${fieldName} should start with a capital letter!`);
        return false;
    } else if (!/^[a-zA-Zа-яА-Я]+$/.test(name)) {
        $(`#${isSurname ? 'surname' : 'name'}-error`).css('display', 'block');
        $(`#${isSurname ? 'surname' : 'name'}-error`).text(`${fieldName} should contain only letters!`);
        return false;
    } else {
        $(`#${isSurname ? 'surname' : 'name'}-error`).text('');
        return true;
    }
}

function isValidDate(birthDate)
{
    if (!birthDate) {
        $(`#birthday-error`).css('display', 'block');
        $(`#birthday-error`).text(`Birthday field cannot be empty!`);
        return false;
    }

    const parts = birthDate.split('-');
    const year = parseInt(parts[0], 10);

    const currentYear = new Date().getFullYear();
    if (year < 1990 || year > 2006 || year > currentYear) {
        $(`#birthday-error`).css('display', 'block');
        $(`#birthday-error`).text(`$Birthday date should be from 1990 to 2006!`);
        return false;
    }
    else
    {
        $(`#birthday-error`).text('');
        return true;
    }
}
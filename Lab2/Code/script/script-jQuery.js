function addRow(studentData) {
    var table = $('#student-table');
    var newRow = $('<tr>');

    newRow.append($('<td>').css('display', 'none').text(studentData.id));
    newRow.append($('<td>').css('width', '5%').append('<input type="checkbox">'));
    
    var fullname = studentData.name + ' ' + studentData.surname,
    dateParts = studentData.birthday.split('-'),
    formattedDate = dateParts[2] + '.' + dateParts[1] + '.' + dateParts[0],
    rowData = [studentData.group, fullname, studentData.gender, formattedDate];

    $.each(rowData, function(index, value){
        newRow.append($('<td>').text(value));
    });

    newRow.append($('<td>').html(`
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="${table.find('tr').length % 2 == 0 ? 'grey' : 'green'}" fill-opacity="0.25" class="bi bi-circle-fill" viewBox="0 0 16 16">
            <circle cx="8" cy="8" r="8"/>
        </svg>
    `));

    newRow.append($('<td>').html(`
        <div class="options-div">
            <button onclick="editStudentOnClick(this)" title="Edit" style="text-decoration: none; color: black; padding: 5%;">
                <svg xmlns="http://www.w3.org/2000/svg" width="25%" height="25" fill="currentColor" class="bi bi-pencil-square edit" viewBox="0 0 16 16">
                    <path id="pencil" d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                </svg>
            </button>

            <button title="Delete" style="color: red; padding: 5%;">
                <svg onclick="deleteRow(this)" xmlns="http://www.w3.org/2000/svg" width="25%" height="25" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16">
                    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2z"/>
                    <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708"/>
                </svg>
            </button>
        </div>
    `));

    $("#student-table").append(newRow);
}

$(document).ready(function() {
    function initializeTable() {
        //localStorage.clear();
        fillTableWithLocalData();
    }

    initializeTable();

    var navLinks = $('nav ul li a');

    navLinks.on('click', function() {
        $(this).addClass('active');

        navLinks.not($(this)).removeClass('active');
    });

    $(".close-input-form").click(function(){
        closeInputForm();
    });

    getStudentInfos();
});

function toggleInputForm(isSaveBtn) {
    var overlay = $('#overlay');
    var centeredDiv = $('#form-container');
  
    if (overlay.css('display') === "block") {
        
        var id = $('#studentId').val();
        var group = $('#groupSelect').val();
        var name = $('#nameInput').val();
        var surname = $('#surnameInput').val();
        var gender = $('#genderSelect').val();
        var birthday = $('#birthdayInput').val();

        var studentData = {
            id: id,
            group: group,
            name: name,
            surname: surname,
            gender: gender,
            birthday: birthday
        };

        if (isSaveBtn) {
            var saved = saveData(studentData);
            
            if(saved) {
                setupAddForm();
                overlay.css("display", "none");
                centeredDiv.css("display", "none");
            }else {
                return;
            }
        }
        
    } else {
        overlay.css("display", "block");
        centeredDiv.css("display", "block");
    }
}

function saveData(data) {
    
    if(isValidData(data))
    {
        var jsonData = JSON.stringify(data);
        console.log(jsonData);
        upsertStudent(data);
        return true;
    }
    else {
        return false;
    }
}

function deleteRow(button) {
    var row = $(button).closest('tr');
    showWarningWindow(row);
    enableOverlay();
}

function confirmDelete() {
    var rowIndex = $('#warning-container').data("rowIndex");

    if (rowIndex !== undefined) {
        var table = $('#student-table');
        var row = $(table).find('tr').eq(parseInt(rowIndex));
        row.remove(); 
        var studentId = $(row).find('td:first').text();
        deleteStudent(studentId);
    } else {
        console.error('Рядок не знайдений.');
    }
    
    hideWarningWindow();
    disableOverlay();
}

function setupEditForm(studentData) {
    $('#form-title').text('Edit Student');
    $('#studentId').val(studentData.id);
    $('#groupSelect').val(studentData.group);
    $('#nameInput').val(studentData.name);
    $('#surnameInput').val(studentData.surname);
    $('#genderSelect').val(studentData.gender);
    $('#birthdayInput').val(studentData.birthday);
}

function setupAddForm() {
    $('#form-title').text('Add Student');
    $('#studentId').val('0'); 
    $('#groupSelect').val('');
    $('#nameInput').val('');
    $('#surnameInput').val('');
    $('#genderSelect').val('');
    $('#birthdayInput').val('');
}

function editStudentOnClick(button) {
    toggleInputForm();
    var studentId = $(button).closest('tr').find('td:first').text();
    //getStudentInfoById(studentId);
    var studentData = getStudentInfoLocalStorage(studentId);
    setupEditForm(studentData);
}

function addStudent(studentData) {
    
    let existingText = localStorage.getItem('fileContent') || '';
    const textToAppend = studentData;
    let newText = existingText + '\n' + textToAppend;
    localStorage.setItem('fileContent', newText);
    
    //sendRequest('POST', 'http://127.0.0.1:5500/Code/data.json', studentData);
}

function upsertStudent(studentData) {
    const students = localStorage.getItem('fileContent') || ' ';
    const studentArray = students.split('\n').filter(item => item.trim()); 
    let allStudents = [];

    studentArray.forEach(item => {
        try {
            const parsedItem = JSON.parse(item);
            allStudents.push(parsedItem);
        } catch (error) {
            console.error('Помилка парсингу:', error);
        }
    });

    let foundIndex = -1;
    for (let i = 0; i < allStudents.length; i++) {
        if (allStudents[i].id == studentData.id) {
            foundIndex = i;
            break;
        }
    }

    if (foundIndex === -1) {
        if(allStudents.length == 0)
            studentData.id = 1;
        else
            studentData.id = parseInt(allStudents[allStudents.length - 1].id) + 1;
        allStudents.push(studentData);
    } else {
        allStudents[foundIndex] = studentData;
    }

    const updatedStudentData = allStudents.map(student => JSON.stringify(student)).join('\n');
    localStorage.setItem('fileContent', updatedStudentData);
    console.log(localStorage.getItem('fileContent'));
    fillTableWithLocalData();
}

function getStudentInfoLocalStorage(studentId) {
    const students = localStorage.getItem('fileContent') || ' ';
    const studentArray = students.split('\n').filter(item => item.trim()); 
    let foundStudent = null;

    studentArray.forEach(item => {
        try {
            const parsedItem = JSON.parse(item);
            if (parsedItem.id == studentId) {
                foundStudent = parsedItem;
                return;
            }
        } catch (error) {
            console.error('Помилка парсингу:', error);
        }
    });

    return foundStudent;
}

function deleteStudent(studentId) {
    const students = localStorage.getItem('fileContent') || ' ';
    const studentArray = students.split('\n').filter(item => item.trim()); 
    let allStudents = [];

    studentArray.forEach(item => {
        try {
            const parsedItem = JSON.parse(item);
            allStudents.push(parsedItem);
        } catch (error) {
            console.error('Помилка парсингу:', error);
        }
    });

    const updatedStudents = allStudents.filter(student => student.id != studentId);
    const updatedStudentData = updatedStudents.map(student => JSON.stringify(student)).join('\n');
    localStorage.setItem('fileContent', updatedStudentData);
    console.log(localStorage.getItem('fileContent'));
    fillTableWithLocalData();
}

function fillTableWithLocalData() {
    var table = $('#student-table');
    var firstRow = table.find('tr:first');
    table.find('tr').not(firstRow).remove();

    const students = localStorage.getItem('fileContent') || ' ';
    const studentArray = students.split('\n').filter(item => item.trim()); 
    let allStudents = [];

    studentArray.forEach(item => {
        try {
            var parsed = JSON.parse(item);
            allStudents.push(parsed);
        } catch (error) {
            console.error('Помилка парсингу:', error);
        }
    });

    for (let i = 0; i < allStudents.length; i++) {
        addRow(allStudents[i]);
    }
}

function getStudentInfoById(studentId)
{
    $.ajax({
        url: 'http://127.0.0.1:5500/Code/data.json',
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            var studentData = null;
            
            for (var i = 0; i < response.length; i++) {
                if (response[i].id == studentId) {
                    studentData = response[i];
                    break;
                }
            }
    
            if(studentData != null){
                setupEditForm(studentData);
            } else {
                console.log('Студент з ID ' + studentId + ' не знайдений.');
            }
        },
        error: function(xhr, status, error) {
            console.alert('Помилка отримання даних. Статус:', xhr, status, error);
        }
    });
}

function getStudentInfos() {
    $.ajax({
        url: 'http://127.0.0.1:5500/Code/data.json',
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            console.log(response);
            //callback(response);
        },
        error: function(xhr, status, error) {
            console.error('Помилка отримання даних. Статус:', xhr, status, error);
        }
    });
}

function sendRequest(method, url, body = null)
{
    const headers = {
        'Content-Type': 'application/json'
    };

    return fetch(url, {
        method: method,
        body: body,
        headers: headers
    }).then(response => {
        if(response.ok)
        {
            alert("sucess" + body);
            return response.json();
        }
        else
        alert("error" + body);
    });
}
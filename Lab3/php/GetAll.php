<?php

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    // Зчитування вмісту файлу data.json
    $json_data = file_get_contents('data.json');

    // Переведення JSON рядка у асоціативний масив PHP
    $data = json_decode($json_data, true);

    // Повернення всіх даних у форматі JSON
    echo json_encode($data);
} else {
    // Якщо метод запиту не GET, відправити повідомлення про помилку
    http_response_code(405); // Помилка 405 - неприпустимий метод
    echo json_encode(array('error' => 'Метод не підтримується'));
}

?>

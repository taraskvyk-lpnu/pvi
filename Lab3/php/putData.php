<?php
function isValidData($studentData) {
    $isValid = true;

    if (!isValidGroup($studentData['group'])) {
        $isValid = false;
    }

    if (!isValidName($studentData['name'], false)) {
        $isValid = false;
    }
    
    if (!isValidName($studentData['surname'], true)) {
        $isValid = false;
    
    }
    if (!isValidGender($studentData['gender'])) {
        $isValid = false;
    }

    if (!isValidDate($studentData['birthday'])) {
        $isValid = false;
    }

    return $isValid;
}

function isValidGroup($group) {
    if (!$group) {
        $response = [
            'success' => false,
            'message' => 'Choose group!'
        ];
        echo json_encode($response);
        return false;
    } else {
        return true;
    }
}

function isValidGender($gender) {
    if (!$gender) {
        $response = [
            'success' => false,
            'message' => 'Choose gender!'
        ];
        echo json_encode($response);
        return false;
    } else {
        return true;
    }
}

function isValidName($name, $isSurname) {
    $fieldName = $isSurname ? 'Surname' : 'Name';

    if (!$name) {
        $response = [
            'success' => false,
            'message' => "{$fieldName} field cannot be empty!"
        ];
        echo json_encode($response);
        return false;
    }

    if (strlen($name) < 2) {
        $response = [
            'success' => false,
            'message' => "{$fieldName} should contain at least 2 letters!"
        ];
        echo json_encode($response);
        return false;
    }

    $firstLetter = $name[0];

    if (!preg_match('/^[A-ZА-Я]/u', $firstLetter)) {
        $response = [
            'success' => false,
            'message' => "{$fieldName} should start with a capital letter!"
        ];
        echo json_encode($response);
        return false;
    } else if (!preg_match('/^[a-zA-Zа-яА-Я]+$/', $name)) {
        $response = [
            'success' => false,
            'message' => "{$fieldName} should contain only letters!"
        ];
        echo json_encode($response);
        return false;
    } else {
        return true;
    }
}

function isValidDate($birthDate) {
    if (!$birthDate) {
        $response = [
            'success' => false,
            'message' => 'Birthday field cannot be empty!'
        ];
        echo json_encode($response);
        return false;
    }

    $parts = explode('-', $birthDate);
    $year = intval($parts[0]);

    $currentYear = intval(date('Y'));
    if ($year < 1990 || $year > 2006 || $year > $currentYear) {
        $response = [
            'success' => false,
            'message' => 'Birthday date should be from 1990 to 2006!'
        ];
        echo json_encode($response);
        return false;
    } else {
        return true;
    }
}

// Перевірка методу запиту
if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
    // Отримання даних з тіла запиту
    $data = json_decode(file_get_contents('php://input'), true);

    // Валідація даних
    $errors = array();

    if(isValidData($data)) {
        // Отримання ідентифікатора з параметра запиту
        $id = $_GET['id'];

        // Зчитування даних з файлу (якщо вони є)
        $file = 'data.json';
        $jsonData = file_exists($file) ? json_decode(file_get_contents($file), true) : array();

        // Пошук елемента з відповідним ідентифікатором у масиві
        $index = -1;
        foreach ($jsonData as $key => $item) {
            if ($item['id'] == $id) {
                $index = $key;
                break;
            }
        }

        if ($index !== -1) {
            // Оновлення даних за ідентифікатором
            $jsonData[$index] = $data;

            // Збереження оновлених даних у файл у форматі JSON з відступами
            file_put_contents($file, json_encode($jsonData, JSON_PRETTY_PRINT));

            // Повернення успішного результату у форматі JSON зі статусом 200 (OK)
            http_response_code(200);
            echo json_encode(array("success" => true), JSON_PRETTY_PRINT);
        } else {
            // Повернення помилки, якщо елемент з вказаним ідентифікатором не знайдений
            http_response_code(404);
            echo json_encode(array("success" => false, "error" => "Елемент з ідентифікатором $id не знайдений."), JSON_PRETTY_PRINT);
        }
    } else {
        // Повернення помилок у форматі JSON зі статусом 400 (Bad Request)
        http_response_code(400);
        echo json_encode(array("success" => false, "errors" => $errors), JSON_PRETTY_PRINT);
    }
} else {
    // Повідомлення про помилку, якщо метод запиту не PUT зі статусом 405 (Method Not Allowed)
    http_response_code(405);
    echo json_encode(array("success" => false, "error" => "Invalid request method."), JSON_PRETTY_PRINT);
}

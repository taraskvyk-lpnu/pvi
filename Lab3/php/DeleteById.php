<?php
// Перевірка методу запиту
if ($_SERVER["REQUEST_METHOD"] === "DELETE") {
    // Отримання ідентифікатора студента з запиту
    $studentId = $_GET['id'] ?? null;

    // Перевірка, чи був наданий ідентифікатор студента
    if ($studentId === null) {
        // Відповідь про помилку, якщо ідентифікатор не був наданий
        http_response_code(400); // Помилка: неправильний запит
        echo json_encode(array("success" => false, "error" => "Не вказаний ідентифікатор студента."), JSON_PRETTY_PRINT);
        exit; // Завершення виконання скрипту
    }

    // Зчитування даних з JSON-файлу
    $file = 'data.json';
    $jsonData = file_exists($file) ? json_decode(file_get_contents($file), true) : array();

    // Пошук студента за ідентифікатором та видалення його з масиву
    $deleted = false;
    foreach ($jsonData as $key => $student) {
        if ($student['id'] == $studentId) {
            unset($jsonData[$key]);
            $deleted = true;
            break;
        }
    }

    // Оновлення JSON-файлу з новими даними
    if ($deleted) {
        file_put_contents($file, json_encode(array_values($jsonData), JSON_PRETTY_PRINT));
        // Відповідь про успішне видалення
        http_response_code(200); // ОК
        echo json_encode(array("success" => true), JSON_PRETTY_PRINT);
    } else {
        // Відповідь про помилку, якщо студент з вказаним ідентифікатором не був знайдений
        http_response_code(404); // Помилка: не знайдено
        echo json_encode(array("success" => false, "error" => "Студент з вказаним ідентифікатором не знайдений."), JSON_PRETTY_PRINT);
    }
} else {
    // Відповідь про помилку, якщо метод запиту не DELETE
    http_response_code(405); // Метод не дозволений
    echo json_encode(array("success" => false, "error" => "Неправильний метод запиту. Очікується DELETE."), JSON_PRETTY_PRINT);
}
?>

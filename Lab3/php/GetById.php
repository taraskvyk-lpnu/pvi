<?php

// Перевірка методу запиту
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Отримання ідентифікатора з параметра запиту
    $id = $_GET['id'];

    // Зчитування даних з файлу (якщо вони є)
    $file = 'data.json';
    $jsonData = file_exists($file) ? json_decode(file_get_contents($file), true) : array();

    // Пошук елемента з відповідним ідентифікатором у масиві
    $student = null;
    foreach ($jsonData as $item) {
        if ($item['id'] == $id) {
            $student = $item;
            break;
        }
    }

    if ($student !== null) {
        // Повернення даних студента у форматі JSON зі статусом 200 (OK)
        http_response_code(200);
        echo json_encode($student, JSON_PRETTY_PRINT);
    } else {
        // Повернення помилки, якщо студент з вказаним ідентифікатором не знайдений
        http_response_code(404);
        echo json_encode(array("error" => "Студент з ідентифікатором $id не знайдений."), JSON_PRETTY_PRINT);
    }
} else {
    // Повідомлення про помилку, якщо метод запиту не GET зі статусом 405 (Method Not Allowed)
    http_response_code(405);
    echo json_encode(array("error" => "Неприпустимий метод запиту."), JSON_PRETTY_PRINT);
}

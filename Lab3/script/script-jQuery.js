function addRow(studentData) {
    var table = $('#student-table');
    var newRow = $('<tr>');

    newRow.append($('<td>').css('display', 'none').text(studentData.id));
    newRow.append($('<td>').css('width', '5%').append('<input type="checkbox">'));
    
    var fullname = studentData.name + ' ' + studentData.surname,
    dateParts = studentData.birthday.split('-'),
    formattedDate = dateParts[2] + '.' + dateParts[1] + '.' + dateParts[0],
    rowData = [studentData.group, fullname, studentData.gender, formattedDate];

    $.each(rowData, function(index, value){
        newRow.append($('<td>').text(value));
    });

    newRow.append($('<td>').html(`
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="${table.find('tr').length % 2 == 0 ? 'grey' : 'green'}" fill-opacity="0.25" class="bi bi-circle-fill" viewBox="0 0 16 16">
            <circle cx="8" cy="8" r="8"/>
        </svg>
    `));

    newRow.append($('<td>').html(`
        <div class="options-div">
            <button onclick="editStudentOnClick(this)" title="Edit" style="text-decoration: none; color: black; padding: 5%;">
                <svg xmlns="http://www.w3.org/2000/svg" width="25%" height="25" fill="currentColor" class="bi bi-pencil-square edit" viewBox="0 0 16 16">
                    <path id="pencil" d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                </svg>
            </button>

            <button title="Delete" style="color: red; padding: 5%;">
                <svg onclick="deleteRow(this)" xmlns="http://www.w3.org/2000/svg" width="25%" height="25" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16">
                    <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2z"/>
                    <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708"/>
                </svg>
            </button>
        </div>
    `));

    $("#student-table").append(newRow);
}55

$(document).ready(function() {
    function initializeTable() {
        fillTableWithJsonData();
    }

    initializeTable();

    var navLinks = $('nav ul li a');

    navLinks.on('click', function() {
        $(this).addClass('active');

        navLinks.not($(this)).removeClass('active');
    });

    $(".close-input-form").click(function(){
        closeInputForm();
    });
});

async function toggleInputForm(isSaveBtn = true) {
    var overlay = $('#overlay');
    var centeredDiv = $('#form-container');
  
    if (overlay.css('display') === "block") {
        
        var id = $('#studentId').val();
        var group = $('#groupSelect').val();
        var name = $('#nameInput').val();
        var surname = $('#surnameInput').val();
        var gender = $('#genderSelect').val();
        var birthday = $('#birthdayInput').val();

        var studentData = {
            id: id,
            group: group,
            name: name,
            surname: surname,
            gender: gender,
            birthday: birthday
        };

        if (isSaveBtn) {     
            var saved;

            try{
                saved = await saveData(studentData);
            }
            catch (error) {
                alertError(error);
            }
            
            if(saved) {
                setupAddForm();
                //overlay.css("display", "none");
                //centeredDiv.css("display", "none");
            }else {
                return;
            }
        }
        
    } else {
        overlay.css("display", "block");
        centeredDiv.css("display", "block");
    }
}

async function postStudent(studentData) {
    try {
        const responses = await $.ajax({
            url: "/php/server.php",
            type: "POST",
            data: JSON.stringify(studentData),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        });
        
        closeInputForm();
        return true;

    } catch (error) {
        const responses = JSON.parse(error.responseText);
        let message = '';

        responses.forEach(function(item) {
            message += item.message + '\n'; 
        });
        
        alert(message);
        console.error("An error occurred:", error.responseText);
    }
}

async function saveData(data) {
    
    if(isValidData(data))
    {
        var jsonData = JSON.stringify(data);
        console.log(jsonData);

        try{
            await upsertStudent(data);
            console.log('After upsert');
        }
        catch (error) {
            alertError(error);
            return;
        }
        return true;
    }
    else {
        return false;
    }
}

function alertError(error){
    const errorMessage = error.responseText;

    if(!errorMessage)
        return;

    const errorMessages = errorMessage.split('}');
    let finalMessage = ''; 
    
    errorMessages.forEach((errorMessageJSON) => {
        if (errorMessageJSON.trim() !== '') {
            const errorObject = JSON.parse(errorMessageJSON + '}');
            finalMessage += errorObject.message + '\n'; 
        }
    });

    console.log(finalMessage);
    alert(finalMessage);
}

function deleteRow(button) {
    var row = $(button).closest('tr');
    showWarningWindow(row);
    enableOverlay();
}

async function confirmDelete() {
    var rowIndex = $('#warning-container').data("rowIndex");

    if (rowIndex !== undefined) {
        var table = $('#student-table');
        var row = $(table).find('tr').eq(parseInt(rowIndex));
        row.remove(); 
        var studentId = $(row).find('td:first').text();
        await deleteStudentAsync(studentId);
    } else {
        console.error('Рядок не знайдений.');
    }
    
    hideWarningWindow();
    disableOverlay();
}

function setupEditForm(studentData) {
    $('#form-title').text('Edit Student');
    $('#studentId').val(studentData.id);
    $('#groupSelect').val(studentData.group);
    $('#nameInput').val(studentData.name);
    $('#surnameInput').val(studentData.surname);
    $('#genderSelect').val(studentData.gender);
    $('#birthdayInput').val(studentData.birthday);
}

function setupAddForm() {
    $('#form-title').text('Add Student');
    $('#studentId').val(0); 
    $('#groupSelect').val('');
    $('#nameInput').val('');
    $('#surnameInput').val('');
    $('#genderSelect').val('');
    $('#birthdayInput').val('');
}

async function editStudentOnClick(button) {
    toggleInputForm();
    var studentId = $(button).closest('tr').find('td:first').text();
    console.log(studentId);
    
    try {
        var studentInfo  = await getStudentById(studentId);
        console.log(studentInfo);
        setupEditForm(studentInfo);
    } catch (error) {
        console.error('Помилка обробки інформації про студента:', error);
    }
}

async function updateStudentById(studentId, updatedData) {
    console.log('update');
    console.log(studentId);
    console.log(updatedData);
    console.log('put');
    const url = `http://localhost:3000/php/putData.php?id=${studentId}`;
    const response = await $.ajax({
        url: url,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(updatedData),
        dataType: 'json'
    });
    
    console.log('success');

    if (response.success) {
        closeInputForm();
        setupAddForm();
        console.log('success');
    } else {
        alert(response.message.responseText);
        // Handle error case
        alert("Failed to save data.");
    }

    console.log('Успішно оновлено:', response);
    try {
    } catch (error) {
        alertError(error);
        console.log('error');
    }
}

async function upsertStudent(studentData) {
    console.log(studentData);
    console.log(studentData.id);
    
    if(studentData.id == 0) {
        console.log('post');
        await postStudent(studentData);    
    }
    else {
        //PUT запит
        console.log('put11');
        console.log(studentData.id);
        console.log(studentData);
        console.log('after put');
        await updateStudentById(studentData.id, studentData);
    }

    await fillTableWithJsonData();
}

async function getStudentById(studentId) {
    try {
        const response = await $.ajax({
            url: `http://localhost:3000/php/GetById.php?id=${studentId}`,
            type: 'GET',
            dataType: 'json'
        });

        console.log('GET by Id');
        console.log(response);
        
        return response;
        // Обробка отриманих даних
    } catch (error) {
        alertError(error);
        // Обробка помилки
    }
}

async function deleteStudentAsync(studentId) {
    try {
        const response = await $.ajax({
            url: `http://localhost:3000/php/DeleteById.php?id=${studentId}`, // Замініть URL на потрібний
            type: 'DELETE',
            dataType: 'json'
        });

        console.log(response); // Виведення результату у консоль для дебагу
        // Обробка результату видалення
    } catch (error) {
        console.error('Помилка видалення студента:', error);
        // Обробка помилки
    }
}

async function fillTableWithJsonData() {
    var table = $('#student-table');
    var firstRow = table.find('tr:first');
    table.find('tr').not(firstRow).remove();

    try {
        const allStudentsData = await getStudentInfos();
        allStudentsData.forEach(function(student) {
            addRow(student);
        });

    } catch (error) {
        alertError(error);
    }
}

async function getStudentInfos() {
    try {
        const response = await $.ajax({
            url: 'http://localhost:3000/php/GetAll.php',
            type: 'GET',
            dataType: 'json'
        });
 
        return response;
        
    } catch (error) {
        alertError(error);
    }
}